
package org.clases;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import java.util.Date;
import javax.swing.Icon;
import javax.swing.ImageIcon;



public class Frame extends javax.swing.JFrame{

    Image imagen;
    ImageIcon icono;

    public Frame() {
        initComponents();
        this.setIconImage(new ImageIcon(getClass().getResource("/imagenes/logo.jpg")).getImage());
        icono = new ImageIcon(this.getClass().getResource("/imagenes/logo.jpg"));
        imagen = icono.getImage();
        lblIcon.setIcon(new ImageIcon(imagen.getScaledInstance(lblIcon.getWidth(), lblIcon.getHeight(), WIDTH)));
        try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String sql="SELECT * FROM usuarios";
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                cbIDUsuarioCT.addItem(rs.getString(1));       
            }    
        } catch (SQLException ex) {
        }
        
        try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String sql="SELECT * FROM clientes";
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                cbIDClienteVH.addItem(rs.getString(2));       
            }    
        } catch (SQLException ex) {
        }
        
        
        try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String sql="SELECT * FROM piezas";
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                cbPiezaRP.addItem(rs.getString(1));       
            }    
        } catch (SQLException ex) {
        } 
        
        try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String sql="SELECT * FROM vehiculos";
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                cbVehiculoRP.addItem(rs.getString(2));       
            }    
        } catch (SQLException ex) {
        }
        
        
       
        try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String sql="SELECT * FROM reparaciones";
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                if(rs.getString(10).equals("NO PAGADO"))
                    cbIDRPVT.addItem(rs.getString(2)); 

            }    
        } catch (SQLException ex) {
        } 
        
        cbIDUsuarioCT.setSelectedIndex(-1);
        cbIDClienteVH.setSelectedIndex(-1);
        cbPiezaRP.setSelectedIndex(-1);
        cbVehiculoRP.setSelectedIndex(-1);
        cbIDRPVT.setSelectedIndex(-1);

    }
    

    
     //Habilitar/Deshabilitar
    public void HabilitarTextFieldUsuarios(){
        txtNombreUS.setEnabled(true);
        txtAPUS.setEnabled(true);
        txtAMUS.setEnabled(true);
        txtTelefonoUS.setEnabled(true);
        txtDireccionUS.setEnabled(true);
    }
    public void DesHabilitarTextFieldUsuarios(){
        txtNombreUS.setEnabled(false);
        txtAPUS.setEnabled(false);
        txtAMUS.setEnabled(false);
        txtTelefonoUS.setEnabled(false);
        txtDireccionUS.setEnabled(false);  
        txtIDUS.setEnabled(false);
    }
    
    public void HabilitarTextFieldClientes(){
        txtNombreCT.setEnabled(true);
        txtAPCT.setEnabled(true);
        txtAMCT.setEnabled(true);
        txtDireccionCT.setEnabled(true);
        cbIDUsuarioCT.setEnabled(true);

    }
    public void DesHabilitarTextFieldClientes(){
        txtNombreCT.setEnabled(false);
        txtAPCT.setEnabled(false);
        txtAMCT.setEnabled(false);
        txtCURPCT.setEnabled(false);
        txtDireccionCT.setEnabled(false);
        cbIDUsuarioCT.setEnabled(false);
    }
 
    public void HabilitarTextFieldVehiculos(){
        txtMatriculaVH.setEnabled(true);
        txtMarcaVH.setEnabled(true);
        txtModeloVH.setEnabled(true);
        cbIDClienteVH.setEnabled(true);
    }
    public void DesHabilitarTextFieldVehiculos(){
        txtMatriculaVH.setEnabled(false);
        txtMarcaVH.setEnabled(false);
        txtModeloVH.setEnabled(false);
        txtFechaVH.setEnabled(false);
        cbIDClienteVH.setEnabled(false);
    }
    
    public void HabilitarTextFieldReparaciones(){
        txtFallaRP.setEnabled(true);
        txtFechaENRP.setEnabled(true);
        txtFechaSARP.setEnabled(true);
        txtCantidadPiezasRP.setEnabled(true);
        cbVehiculoRP.setEnabled(true);
        cbPiezaRP.setEnabled(true);
        txtMontoServicio.setEnabled(true);   
        cbTipoServicioRP.setEnabled(true);
    }
    public void DesHabilitarTextFieldReparaciones(){
        txtFallaRP.setEnabled(false);
        txtFechaENRP.setEnabled(false);
        txtFechaSARP.setEnabled(false);  
        txtCantidadPiezasRP.setEnabled(false);
        cbVehiculoRP.setEnabled(false);
        cbPiezaRP.setEnabled(false);
        txtMontoServicio.setEnabled(false);
        cbTipoServicioRP.setEnabled(false);
    }
   
    public void HabilitarTextFieldPiezas(){
        txtDescripcionPZ.setEnabled(true);
        txtNombrePZ.setEnabled(true);
        txtStockPZ.setEnabled(true); 
        txtPrecioPZ.setEnabled(true);
    }
    public void DesHabilitarTextFieldPiezas(){
        txtDescripcionPZ.setEnabled(false);
        txtNombrePZ.setEnabled(false);
        txtStockPZ.setEnabled(false);   
        txtPrecioPZ.setEnabled(false);
        txtIDPZ.setEnabled(false);
    }
   
    public void HabilitarTextFieldVenta(){      
        cbIDRPVT.setEnabled(true);
        cbTipoPagoVT.setEnabled(true);
        txtFechaPagoVT.setEnabled(true);
    }
    public void DesHabilitarTextFieldVenta(){
        cbIDRPVT.setEnabled(false);
        cbTipoPagoVT.setEnabled(false);
        txtFechaPagoVT.setEnabled(false);
    }
       
    public void HabilitarTextFieldCobro(){
        cbIDRPVT.setEnabled(true);
        txtFechaPagoVT.setEnabled(true);
        cbTipoPagoVT.setEnabled(true);
    }
    public void DesHabilitarTextFieldCobro(){
        cbIDRPVT.setEnabled(false);
        txtFechaPagoVT.setEnabled(false);
        cbTipoPagoVT.setEnabled(false);     
    }
    
    //Limpiar
    public void LimpiarUsuarios(){
        txtIDUS.setText("");
        txtNombreUS.setText("");
        txtAPUS.setText("");
        txtAMUS.setText("");
        txtTelefonoUS.setText("");
        txtDireccionUS.setText("");
        txtDatosUS.setText("");
    }
    public void LimpiarClientes(){
        txtCURPCT.setText("");
        txtNombreCT.setText("");
        txtAPCT.setText("");
        txtAMCT.setText("");
        txtDatosCT.setText("");
        txtDireccionCT.setText("");
        txtFechaCT.setText("");
        
        cbIDUsuarioCT.removeAllItems();

        try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String sql="SELECT * FROM usuarios";
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                cbIDUsuarioCT.addItem(rs.getString(1));       
            }    
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,"Hubo un error con la búsqueda");
        } 
        
        cbIDUsuarioCT.setSelectedIndex(-1);
        
        cbIDClienteVH.removeAllItems();     
        
        try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String sql="SELECT * FROM clientes";
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                cbIDClienteVH.addItem(rs.getString(2));       
            }    
        } catch (SQLException ex) {
        } 
        
        
        cbIDClienteVH.setSelectedIndex(-1);
        
    }
    public void LimpiarVehiculos(){
        txtMatriculaVH.setText("");
        txtMarcaVH.setText("");
        txtModeloVH.setText("");  
        txtFechaVH.setText("");
        txtDatosVH.setText("");
        
        cbVehiculoRP.removeAllItems();
        
        try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String sql="SELECT * FROM vehiculos";
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                cbVehiculoRP.addItem(rs.getString(2));       
            }    
        } catch (SQLException ex) {
        } 
        
        cbVehiculoRP.setSelectedIndex(-1);
        
        cbIDClienteVH.removeAllItems();
        
        try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String sql="SELECT * FROM clientes";
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                cbIDClienteVH.addItem(rs.getString(2));       
            }    
        } catch (SQLException ex) {
        } 
        
        
        cbIDClienteVH.setSelectedIndex(-1);
    }
    public void LimpiarReparacion(){
        txtFallaRP.setText("");
        txtFechaENRP.setText("");
        txtFechaSARP.setText("");
        txtCantidadPiezasRP.setText("");
        txtIDRP.setText("");
        txtDatosRP.setText("");
        txtMontoServicio.setText("");
        txtMontoPZ.setText("");
        txtEstatusRP.setText("");
        cbTipoServicioRP.removeAllItems();
        
        cbVehiculoRP.removeAllItems();
        try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String sql="SELECT * FROM vehiculos";
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                cbVehiculoRP.addItem(rs.getString(2));       
            }    
        } catch (SQLException ex) {
        } 

        cbVehiculoRP.setSelectedIndex(-1);
        
        
        cbPiezaRP.removeAllItems();
        try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String sql="SELECT * FROM piezas";
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                cbPiezaRP.addItem(rs.getString(1));       
            }    
        } catch (SQLException ex) {
        } 

        cbPiezaRP.setSelectedIndex(-1);   
        
        
        cbIDRPVT.removeAllItems();
        try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String sql="SELECT * FROM reparaciones";
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                cbIDRPVT.addItem(rs.getString(3));       
            }    
        } catch (SQLException ex) {
        }
        cbIDRPVT.setSelectedIndex(-1);
        
    }
    public void LimpiarPieza(){
        txtDescripcionPZ.setText("");
        txtIDPZ.setText("");
        txtStockPZ.setText(""); 
        txtNombrePZ.setText("");
        txtDatosPZ.setText("");
        txtPrecioPZ.setText("");
        
        cbPiezaRP.removeAllItems();
        
        try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String sql="SELECT * FROM piezas";
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                cbPiezaRP.addItem(rs.getString(1));       
            }    
        } catch (SQLException ex) {
        } 
        
        cbPiezaRP.setSelectedIndex(-1);       
    } 
    public void LimpiarCobro(){
        txtIDVT.setText("");
        txtTotalVT.setText("");
        txtDatosVT.setText("");
        txtFechaCobroVT.setText("");
        txtFechaPagoVT.setText("");
        txtFechaPagoVT.setText("");
        txtClienteVT.setText("");
        txtEstatusCB.setText("");
    
        
        cbIDRPVT.removeAllItems();
        try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String sql="SELECT * FROM reparaciones";
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                if(rs.getString(10).equals("NO PAGADO"))
                    cbIDRPVT.addItem(rs.getString(3)); 

            }    
        } catch (SQLException ex) {
        } 
        cbIDRPVT.setSelectedIndex(-1); 
        
        
        cbTipoPagoVT.removeAllItems();
        
        cbTipoPagoVT.setSelectedIndex(-1); 
    }
    
    //True
    public void NuevoTrueUsuarios(){
        btnNuevoUsuario.setEnabled(true);
        btnSalvarUsuario.setEnabled(false);
        btnCancelarUsuario.setEnabled(false);
        btnEditarUsuario.setEnabled(false);
        btnRemoverUsuario.setEnabled(false);
    }
    public void NuevoTrueClientes(){
        btnNuevoCliente.setEnabled(true);
        btnSalvarCliente.setEnabled(false);
        btnCancelarCliente.setEnabled(false);
        btnEditarCliente.setEnabled(false);
        btnRemoverCliente.setEnabled(false);
        
    }
    public void NuevoTrueVehiculo(){
        btnNuevoVehiculo.setEnabled(true);
        btnSalvarVehiculo.setEnabled(false);
        btnCancelarVehiculo.setEnabled(false);
        btnEditarVehiculo.setEnabled(false);
        btnRemoverVehiculo.setEnabled(false);
    }
    public void NuevoTrueReparacion(){
        btnNuevoRP.setEnabled(true);
        btnSalvarRP.setEnabled(false);
        btnCancelarRP.setEnabled(false);
        btnEditarRP.setEnabled(false);
        btnRemoverRP.setEnabled(false);
        
    }
    public void NuevoTruePieza(){
        btnNuevoPieza.setEnabled(true);
        btnSalvarPieza.setEnabled(false);
        btnCancelarPieza.setEnabled(false);
        btnEditarPieza.setEnabled(false);
        btnRemoverPieza.setEnabled(false);
    }
    public void NuevoTrueCobro(){
        btnNuevoVT.setEnabled(true);
        btnSalvarVT.setEnabled(false);
        btnCancelarVT.setEnabled(false);
        btnEditarVT.setEnabled(false);
        btnRemoverVT.setEnabled(false);
    }
     
    //Validar palabra
    public boolean ValidarP(String text) {
        try { 
           Integer.parseInt(text); 
        } catch(Exception ex) { 
           return false; 
        }
        JOptionPane.showMessageDialog(rootPane, "Error, palabra ingresada no valida");
        return true;
    }
    
    //Generar Fecha
    public String GenerarFecha(){
        Date fecha = new Date();
        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/YYYY");
        return formatoFecha.format(fecha);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        lblIDUsuarios = new javax.swing.JLabel();
        txtIDUS = new javax.swing.JTextField();
        lblNombreUS = new javax.swing.JLabel();
        txtNombreUS = new javax.swing.JTextField();
        lblAPUsuarios = new javax.swing.JLabel();
        txtAPUS = new javax.swing.JTextField();
        txtAMUS = new javax.swing.JTextField();
        txtTelefonoUS = new javax.swing.JTextField();
        lblAMUsuarios = new javax.swing.JLabel();
        lblTelefonoUsuarios = new javax.swing.JLabel();
        lblDireccionUsuarios = new javax.swing.JLabel();
        txtDireccionUS = new javax.swing.JTextField();
        btnNuevoUsuario = new javax.swing.JButton();
        btnSalvarUsuario = new javax.swing.JButton();
        btnCancelarUsuario = new javax.swing.JButton();
        btnEditarUsuario = new javax.swing.JButton();
        btnRemoverUsuario = new javax.swing.JButton();
        btnBuscarUsuario = new javax.swing.JButton();
        lblBuscarUsuarios = new javax.swing.JLabel();
        txtDatosUS = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        lblIcon = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        lblIDClientesUsuario = new javax.swing.JLabel();
        lblIDCliente = new javax.swing.JLabel();
        txtCURPCT = new javax.swing.JTextField();
        txtAPCT = new javax.swing.JTextField();
        lblAPClientes = new javax.swing.JLabel();
        txtAMCT = new javax.swing.JTextField();
        lblAMClientes = new javax.swing.JLabel();
        btnNuevoCliente = new javax.swing.JButton();
        btnSalvarCliente = new javax.swing.JButton();
        btnCancelarCliente = new javax.swing.JButton();
        btnEditarCliente = new javax.swing.JButton();
        btnRemoverCliente = new javax.swing.JButton();
        lblIDBuscarClientes = new javax.swing.JLabel();
        txtDatosCT = new javax.swing.JTextField();
        btnBuscarCliente = new javax.swing.JButton();
        lblNombreCliente = new javax.swing.JLabel();
        txtNombreCT = new javax.swing.JTextField();
        cbIDUsuarioCT = new javax.swing.JComboBox<>();
        lblDireccionCT = new javax.swing.JLabel();
        txtDireccionCT = new javax.swing.JTextField();
        lblNOMBREODICT = new javax.swing.JLabel();
        lblFechaAtencionCT = new javax.swing.JLabel();
        txtFechaCT = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        lblMatriculaVehiculo = new javax.swing.JLabel();
        txtMatriculaVH = new javax.swing.JTextField();
        lblSeleccionarVehiculo = new javax.swing.JLabel();
        txtMarcaVH = new javax.swing.JTextField();
        lblMarcaVehiculo = new javax.swing.JLabel();
        lblModeloVehiculo = new javax.swing.JLabel();
        txtModeloVH = new javax.swing.JTextField();
        lblFechaVehiculo = new javax.swing.JLabel();
        txtFechaVH = new javax.swing.JTextField();
        btnNuevoVehiculo = new javax.swing.JButton();
        btnSalvarVehiculo = new javax.swing.JButton();
        btnCancelarVehiculo = new javax.swing.JButton();
        btnEditarVehiculo = new javax.swing.JButton();
        btnRemoverVehiculo = new javax.swing.JButton();
        jLabel21 = new javax.swing.JLabel();
        txtDatosVH = new javax.swing.JTextField();
        btnBuscarVehiculo = new javax.swing.JButton();
        cbIDClienteVH = new javax.swing.JComboBox<>();
        lblNOMBREOIDVH = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        btnNuevoPieza = new javax.swing.JButton();
        btnSalvarPieza = new javax.swing.JButton();
        btnCancelarPieza = new javax.swing.JButton();
        btnEditarPieza = new javax.swing.JButton();
        btnRemoverPieza = new javax.swing.JButton();
        lblIDBuscarPieza = new javax.swing.JLabel();
        txtDatosPZ = new javax.swing.JTextField();
        lblIDPieza = new javax.swing.JLabel();
        txtIDPZ = new javax.swing.JTextField();
        lblDescripcionPieza = new javax.swing.JLabel();
        txtDescripcionPZ = new javax.swing.JTextField();
        lblStockPieza = new javax.swing.JLabel();
        txtStockPZ = new javax.swing.JTextField();
        btnBuscarPiezas = new javax.swing.JButton();
        lblNombrePZ = new javax.swing.JLabel();
        txtNombrePZ = new javax.swing.JTextField();
        lblFondoUS = new javax.swing.JLabel();
        lbl = new javax.swing.JLabel();
        lblPrecioPZ = new javax.swing.JLabel();
        txtPrecioPZ = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        lblIDVehiculoReparaciones = new javax.swing.JLabel();
        cbVehiculoRP = new javax.swing.JComboBox();
        txtIDRP = new javax.swing.JTextField();
        btnNuevoRP = new javax.swing.JButton();
        btnSalvarRP = new javax.swing.JButton();
        btnCancelarRP = new javax.swing.JButton();
        btnEditarRP = new javax.swing.JButton();
        btnRemoverRP = new javax.swing.JButton();
        jLabel23 = new javax.swing.JLabel();
        txtDatosRP = new javax.swing.JTextField();
        lblReparacionID = new javax.swing.JLabel();
        txtFechaENRP = new javax.swing.JTextField();
        lblFechaEntradaReparaciones = new javax.swing.JLabel();
        lblFechaSalidaReparaciones = new javax.swing.JLabel();
        txtFechaSARP = new javax.swing.JTextField();
        lblPiezaIDreaparaciones = new javax.swing.JLabel();
        lblFallaReparaciones = new javax.swing.JLabel();
        txtFallaRP = new javax.swing.JTextField();
        btnBuscarReparaciones = new javax.swing.JButton();
        lblCantidadPiezasReparaciones = new javax.swing.JLabel();
        cbPiezaRP = new javax.swing.JComboBox<>();
        lblNOMBREOIDRP = new javax.swing.JLabel();
        lblMonto1 = new javax.swing.JLabel();
        lblMonto2 = new javax.swing.JLabel();
        txtMontoServicio = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtMontoPZ = new javax.swing.JTextField();
        txtCantidadPiezasRP = new javax.swing.JTextField();
        lblEstatusRP = new javax.swing.JLabel();
        txtEstatusRP = new javax.swing.JTextField();
        lblTipoServicio = new javax.swing.JLabel();
        cbTipoServicioRP = new javax.swing.JComboBox<>();
        jPanel5 = new javax.swing.JPanel();
        lblIDVT = new javax.swing.JLabel();
        lblReparacionIDVT = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        lblTipoPago = new javax.swing.JLabel();
        cbIDRPVT = new javax.swing.JComboBox<>();
        txtIDVT = new javax.swing.JTextField();
        txtTotalVT = new javax.swing.JTextField();
        cbTipoPagoVT = new javax.swing.JComboBox<>();
        btnNuevoVT = new javax.swing.JButton();
        btnSalvarVT = new javax.swing.JButton();
        btnCancelarVT = new javax.swing.JButton();
        btnEditarVT = new javax.swing.JButton();
        btnRemoverVT = new javax.swing.JButton();
        btnBuscarVT = new javax.swing.JButton();
        lblDatosVT = new javax.swing.JLabel();
        txtDatosVT = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        lblFechaVT = new javax.swing.JLabel();
        txtFechaCobroVT = new javax.swing.JTextField();
        lblFechaPago = new javax.swing.JLabel();
        txtFechaPagoVT = new javax.swing.JTextField();
        lblClienteVT = new javax.swing.JLabel();
        txtClienteVT = new javax.swing.JTextField();
        lblEstatusCB = new javax.swing.JLabel();
        txtEstatusCB = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Taller Automotríz \"Mojarro\"");
        setBackground(new java.awt.Color(255, 255, 255));
        setResizable(false);

        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setPreferredSize(new java.awt.Dimension(850, 472));
        jTabbedPane1.setRequestFocusEnabled(false);

        jPanel1.setBackground(new java.awt.Color(234, 234, 234));
        jPanel1.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        jPanel1.setLayout(null);

        lblIDUsuarios.setText("ID:");
        jPanel1.add(lblIDUsuarios);
        lblIDUsuarios.setBounds(160, 130, 20, 16);

        txtIDUS.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtIDUS.setEnabled(false);
        jPanel1.add(txtIDUS);
        txtIDUS.setBounds(210, 120, 77, 30);

        lblNombreUS.setText("Nombre:");
        jPanel1.add(lblNombreUS);
        lblNombreUS.setBounds(130, 180, 50, 16);

        txtNombreUS.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtNombreUS.setEnabled(false);
        jPanel1.add(txtNombreUS);
        txtNombreUS.setBounds(210, 170, 250, 30);

        lblAPUsuarios.setText("Apellido Paterno:");
        jPanel1.add(lblAPUsuarios);
        lblAPUsuarios.setBounds(90, 240, 98, 16);

        txtAPUS.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtAPUS.setEnabled(false);
        jPanel1.add(txtAPUS);
        txtAPUS.setBounds(210, 230, 250, 30);

        txtAMUS.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtAMUS.setEnabled(false);
        jPanel1.add(txtAMUS);
        txtAMUS.setBounds(210, 290, 250, 30);

        txtTelefonoUS.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtTelefonoUS.setEnabled(false);
        jPanel1.add(txtTelefonoUS);
        txtTelefonoUS.setBounds(210, 350, 160, 30);

        lblAMUsuarios.setText("Apellido Materno:");
        jPanel1.add(lblAMUsuarios);
        lblAMUsuarios.setBounds(90, 300, 101, 16);

        lblTelefonoUsuarios.setText("Teléfono:");
        jPanel1.add(lblTelefonoUsuarios);
        lblTelefonoUsuarios.setBounds(140, 360, 55, 16);

        lblDireccionUsuarios.setText("Dirección:");
        jPanel1.add(lblDireccionUsuarios);
        lblDireccionUsuarios.setBounds(420, 360, 57, 16);

        txtDireccionUS.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtDireccionUS.setEnabled(false);
        jPanel1.add(txtDireccionUS);
        txtDireccionUS.setBounds(490, 350, 190, 30);

        btnNuevoUsuario.setBackground(new java.awt.Color(255, 255, 255));
        btnNuevoUsuario.setForeground(new java.awt.Color(204, 0, 0));
        btnNuevoUsuario.setText("Nuevo");
        btnNuevoUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoUsuarioActionPerformed(evt);
            }
        });
        jPanel1.add(btnNuevoUsuario);
        btnNuevoUsuario.setBounds(200, 420, 100, 40);

        btnSalvarUsuario.setBackground(new java.awt.Color(255, 255, 255));
        btnSalvarUsuario.setForeground(new java.awt.Color(204, 0, 0));
        btnSalvarUsuario.setText("Salvar");
        btnSalvarUsuario.setEnabled(false);
        btnSalvarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarUsuarioActionPerformed(evt);
            }
        });
        jPanel1.add(btnSalvarUsuario);
        btnSalvarUsuario.setBounds(310, 420, 90, 40);

        btnCancelarUsuario.setBackground(new java.awt.Color(255, 255, 255));
        btnCancelarUsuario.setForeground(new java.awt.Color(204, 0, 0));
        btnCancelarUsuario.setText("Cancelar");
        btnCancelarUsuario.setEnabled(false);
        btnCancelarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarUsuarioActionPerformed(evt);
            }
        });
        jPanel1.add(btnCancelarUsuario);
        btnCancelarUsuario.setBounds(410, 420, 90, 40);

        btnEditarUsuario.setBackground(new java.awt.Color(255, 255, 255));
        btnEditarUsuario.setForeground(new java.awt.Color(204, 0, 0));
        btnEditarUsuario.setText("Editar");
        btnEditarUsuario.setEnabled(false);
        btnEditarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarUsuarioActionPerformed(evt);
            }
        });
        jPanel1.add(btnEditarUsuario);
        btnEditarUsuario.setBounds(510, 420, 100, 40);

        btnRemoverUsuario.setBackground(new java.awt.Color(255, 255, 255));
        btnRemoverUsuario.setForeground(new java.awt.Color(204, 0, 0));
        btnRemoverUsuario.setText("Remover");
        btnRemoverUsuario.setEnabled(false);
        btnRemoverUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverUsuarioActionPerformed(evt);
            }
        });
        jPanel1.add(btnRemoverUsuario);
        btnRemoverUsuario.setBounds(620, 420, 90, 40);

        btnBuscarUsuario.setBackground(new java.awt.Color(255, 255, 255));
        btnBuscarUsuario.setForeground(new java.awt.Color(204, 0, 0));
        btnBuscarUsuario.setText("Buscar");
        btnBuscarUsuario.setActionCommand("");
        btnBuscarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarUsuarioActionPerformed(evt);
            }
        });
        jPanel1.add(btnBuscarUsuario);
        btnBuscarUsuario.setBounds(680, 50, 71, 30);

        lblBuscarUsuarios.setText("Ingrese registro a Buscar:");
        jPanel1.add(lblBuscarUsuarios);
        lblBuscarUsuarios.setBounds(300, 60, 157, 16);

        txtDatosUS.setDisabledTextColor(new java.awt.Color(242, 242, 242));
        jPanel1.add(txtDatosUS);
        txtDatosUS.setBounds(470, 50, 190, 30);

        jLabel1.setText("(Nombre o ID)");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(340, 90, 90, 16);
        jPanel1.add(lblIcon);
        lblIcon.setBounds(530, 100, 320, 240);

        jTabbedPane1.addTab("Empleados", jPanel1);

        jPanel2.setBackground(new java.awt.Color(234, 234, 234));
        jPanel2.setLayout(null);

        lblIDClientesUsuario.setText("Selecione ID Usuario:");
        jPanel2.add(lblIDClientesUsuario);
        lblIDClientesUsuario.setBounds(70, 140, 123, 16);

        lblIDCliente.setText("CURP Cliente:");
        jPanel2.add(lblIDCliente);
        lblIDCliente.setBounds(110, 200, 80, 16);

        txtCURPCT.setEditable(false);
        txtCURPCT.setBackground(new java.awt.Color(255, 255, 255));
        txtCURPCT.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtCURPCT.setEnabled(false);
        jPanel2.add(txtCURPCT);
        txtCURPCT.setBounds(210, 190, 250, 30);

        txtAPCT.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtAPCT.setEnabled(false);
        jPanel2.add(txtAPCT);
        txtAPCT.setBounds(210, 310, 250, 30);

        lblAPClientes.setText("Apellido Paterno:");
        jPanel2.add(lblAPClientes);
        lblAPClientes.setBounds(100, 320, 98, 16);

        txtAMCT.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtAMCT.setEnabled(false);
        jPanel2.add(txtAMCT);
        txtAMCT.setBounds(210, 370, 250, 30);

        lblAMClientes.setText("Apellido Materno:");
        jPanel2.add(lblAMClientes);
        lblAMClientes.setBounds(90, 380, 109, 16);

        btnNuevoCliente.setBackground(new java.awt.Color(255, 255, 255));
        btnNuevoCliente.setForeground(new java.awt.Color(240, 0, 0));
        btnNuevoCliente.setText("Nuevo");
        btnNuevoCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoClienteActionPerformed(evt);
            }
        });
        jPanel2.add(btnNuevoCliente);
        btnNuevoCliente.setBounds(180, 430, 100, 40);

        btnSalvarCliente.setBackground(new java.awt.Color(255, 255, 255));
        btnSalvarCliente.setForeground(new java.awt.Color(240, 0, 0));
        btnSalvarCliente.setText("Salvar");
        btnSalvarCliente.setEnabled(false);
        btnSalvarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarClienteActionPerformed(evt);
            }
        });
        jPanel2.add(btnSalvarCliente);
        btnSalvarCliente.setBounds(290, 430, 90, 40);

        btnCancelarCliente.setBackground(new java.awt.Color(255, 255, 255));
        btnCancelarCliente.setForeground(new java.awt.Color(240, 0, 0));
        btnCancelarCliente.setText("Cancelar");
        btnCancelarCliente.setEnabled(false);
        btnCancelarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarClienteActionPerformed(evt);
            }
        });
        jPanel2.add(btnCancelarCliente);
        btnCancelarCliente.setBounds(390, 430, 90, 40);

        btnEditarCliente.setBackground(new java.awt.Color(255, 255, 255));
        btnEditarCliente.setForeground(new java.awt.Color(240, 0, 0));
        btnEditarCliente.setText("Editar");
        btnEditarCliente.setEnabled(false);
        btnEditarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarClienteActionPerformed(evt);
            }
        });
        jPanel2.add(btnEditarCliente);
        btnEditarCliente.setBounds(490, 430, 100, 40);

        btnRemoverCliente.setBackground(new java.awt.Color(255, 255, 255));
        btnRemoverCliente.setForeground(new java.awt.Color(240, 0, 0));
        btnRemoverCliente.setText("Remover");
        btnRemoverCliente.setEnabled(false);
        btnRemoverCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverClienteActionPerformed(evt);
            }
        });
        jPanel2.add(btnRemoverCliente);
        btnRemoverCliente.setBounds(600, 430, 90, 40);

        lblIDBuscarClientes.setText("Ingrese cliente a Buscar:");
        jPanel2.add(lblIDBuscarClientes);
        lblIDBuscarClientes.setBounds(300, 60, 150, 16);
        jPanel2.add(txtDatosCT);
        txtDatosCT.setBounds(460, 50, 190, 30);

        btnBuscarCliente.setBackground(new java.awt.Color(255, 255, 255));
        btnBuscarCliente.setForeground(new java.awt.Color(240, 0, 0));
        btnBuscarCliente.setText("Buscar");
        btnBuscarCliente.setActionCommand("");
        btnBuscarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarClienteActionPerformed(evt);
            }
        });
        jPanel2.add(btnBuscarCliente);
        btnBuscarCliente.setBounds(660, 50, 71, 30);

        lblNombreCliente.setText("Nombre:");
        jPanel2.add(lblNombreCliente);
        lblNombreCliente.setBounds(140, 260, 50, 16);

        txtNombreCT.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtNombreCT.setEnabled(false);
        jPanel2.add(txtNombreCT);
        txtNombreCT.setBounds(210, 250, 250, 30);

        cbIDUsuarioCT.setEnabled(false);
        jPanel2.add(cbIDUsuarioCT);
        cbIDUsuarioCT.setBounds(210, 130, 130, 30);

        lblDireccionCT.setText("Dirección:");
        jPanel2.add(lblDireccionCT);
        lblDireccionCT.setBounds(510, 260, 60, 16);

        txtDireccionCT.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtDireccionCT.setEnabled(false);
        jPanel2.add(txtDireccionCT);
        txtDireccionCT.setBounds(590, 250, 220, 30);

        lblNOMBREODICT.setText("(CURP)");
        jPanel2.add(lblNOMBREODICT);
        lblNOMBREODICT.setBounds(360, 90, 60, 20);

        lblFechaAtencionCT.setText("Fecha atención:");
        jPanel2.add(lblFechaAtencionCT);
        lblFechaAtencionCT.setBounds(480, 380, 91, 16);

        txtFechaCT.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtFechaCT.setEnabled(false);
        jPanel2.add(txtFechaCT);
        txtFechaCT.setBounds(590, 370, 220, 30);

        jTabbedPane1.addTab("Clientes", jPanel2);

        jPanel3.setBackground(new java.awt.Color(234, 234, 234));
        jPanel3.setLayout(null);

        lblMatriculaVehiculo.setText("Matricula:");
        jPanel3.add(lblMatriculaVehiculo);
        lblMatriculaVehiculo.setBounds(170, 220, 57, 16);

        txtMatriculaVH.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtMatriculaVH.setEnabled(false);
        jPanel3.add(txtMatriculaVH);
        txtMatriculaVH.setBounds(240, 210, 191, 30);

        lblSeleccionarVehiculo.setText("Seleccione Cliente:");
        jPanel3.add(lblSeleccionarVehiculo);
        lblSeleccionarVehiculo.setBounds(110, 150, 109, 16);

        txtMarcaVH.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtMarcaVH.setEnabled(false);
        jPanel3.add(txtMarcaVH);
        txtMarcaVH.setBounds(240, 270, 138, 30);

        lblMarcaVehiculo.setText("Marca:");
        jPanel3.add(lblMarcaVehiculo);
        lblMarcaVehiculo.setBounds(190, 280, 40, 16);

        lblModeloVehiculo.setText("Modelo:");
        jPanel3.add(lblModeloVehiculo);
        lblModeloVehiculo.setBounds(180, 330, 46, 20);

        txtModeloVH.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtModeloVH.setEnabled(false);
        jPanel3.add(txtModeloVH);
        txtModeloVH.setBounds(240, 330, 173, 30);

        lblFechaVehiculo.setText("Fecha:");
        jPanel3.add(lblFechaVehiculo);
        lblFechaVehiculo.setBounds(440, 340, 39, 16);

        txtFechaVH.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtFechaVH.setEnabled(false);
        jPanel3.add(txtFechaVH);
        txtFechaVH.setBounds(490, 330, 154, 30);

        btnNuevoVehiculo.setBackground(new java.awt.Color(255, 255, 255));
        btnNuevoVehiculo.setForeground(new java.awt.Color(204, 0, 0));
        btnNuevoVehiculo.setText("Nuevo");
        btnNuevoVehiculo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoVehiculoActionPerformed(evt);
            }
        });
        jPanel3.add(btnNuevoVehiculo);
        btnNuevoVehiculo.setBounds(160, 420, 100, 40);

        btnSalvarVehiculo.setBackground(new java.awt.Color(255, 255, 255));
        btnSalvarVehiculo.setForeground(new java.awt.Color(204, 0, 0));
        btnSalvarVehiculo.setText("Salvar");
        btnSalvarVehiculo.setEnabled(false);
        btnSalvarVehiculo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarVehiculoActionPerformed(evt);
            }
        });
        jPanel3.add(btnSalvarVehiculo);
        btnSalvarVehiculo.setBounds(270, 420, 90, 40);

        btnCancelarVehiculo.setBackground(new java.awt.Color(255, 255, 255));
        btnCancelarVehiculo.setForeground(new java.awt.Color(204, 0, 0));
        btnCancelarVehiculo.setText("Cancelar");
        btnCancelarVehiculo.setEnabled(false);
        btnCancelarVehiculo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarVehiculoActionPerformed(evt);
            }
        });
        jPanel3.add(btnCancelarVehiculo);
        btnCancelarVehiculo.setBounds(370, 420, 90, 40);

        btnEditarVehiculo.setBackground(new java.awt.Color(255, 255, 255));
        btnEditarVehiculo.setForeground(new java.awt.Color(204, 0, 0));
        btnEditarVehiculo.setText("Editar");
        btnEditarVehiculo.setEnabled(false);
        btnEditarVehiculo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarVehiculoActionPerformed(evt);
            }
        });
        jPanel3.add(btnEditarVehiculo);
        btnEditarVehiculo.setBounds(470, 420, 100, 40);

        btnRemoverVehiculo.setBackground(new java.awt.Color(255, 255, 255));
        btnRemoverVehiculo.setForeground(new java.awt.Color(204, 0, 0));
        btnRemoverVehiculo.setText("Remover");
        btnRemoverVehiculo.setEnabled(false);
        btnRemoverVehiculo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverVehiculoActionPerformed(evt);
            }
        });
        jPanel3.add(btnRemoverVehiculo);
        btnRemoverVehiculo.setBounds(580, 420, 90, 40);

        jLabel21.setText("Ingrese vehículo a Buscar:");
        jPanel3.add(jLabel21);
        jLabel21.setBounds(330, 60, 160, 20);
        jPanel3.add(txtDatosVH);
        txtDatosVH.setBounds(500, 50, 190, 30);

        btnBuscarVehiculo.setBackground(new java.awt.Color(255, 255, 255));
        btnBuscarVehiculo.setForeground(new java.awt.Color(204, 0, 0));
        btnBuscarVehiculo.setText("Buscar");
        btnBuscarVehiculo.setActionCommand("");
        btnBuscarVehiculo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarVehiculoActionPerformed(evt);
            }
        });
        jPanel3.add(btnBuscarVehiculo);
        btnBuscarVehiculo.setBounds(700, 50, 71, 30);

        cbIDClienteVH.setEnabled(false);
        jPanel3.add(cbIDClienteVH);
        cbIDClienteVH.setBounds(240, 140, 220, 30);

        lblNOMBREOIDVH.setText("(Matrícula)");
        jPanel3.add(lblNOMBREOIDVH);
        lblNOMBREOIDVH.setBounds(370, 90, 90, 16);

        jTabbedPane1.addTab("Vehículos", jPanel3);

        jPanel6.setBackground(new java.awt.Color(234, 234, 234));
        jPanel6.setForeground(new java.awt.Color(240, 0, 0));
        jPanel6.setLayout(null);

        btnNuevoPieza.setBackground(new java.awt.Color(255, 255, 255));
        btnNuevoPieza.setForeground(new java.awt.Color(240, 0, 0));
        btnNuevoPieza.setText("Nuevo");
        btnNuevoPieza.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoPiezaActionPerformed(evt);
            }
        });
        jPanel6.add(btnNuevoPieza);
        btnNuevoPieza.setBounds(170, 430, 100, 40);

        btnSalvarPieza.setBackground(new java.awt.Color(255, 255, 255));
        btnSalvarPieza.setForeground(new java.awt.Color(240, 0, 0));
        btnSalvarPieza.setText("Salvar");
        btnSalvarPieza.setEnabled(false);
        btnSalvarPieza.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarPiezaActionPerformed(evt);
            }
        });
        jPanel6.add(btnSalvarPieza);
        btnSalvarPieza.setBounds(280, 430, 90, 40);

        btnCancelarPieza.setBackground(new java.awt.Color(255, 255, 255));
        btnCancelarPieza.setForeground(new java.awt.Color(240, 0, 0));
        btnCancelarPieza.setText("Cancelar");
        btnCancelarPieza.setEnabled(false);
        btnCancelarPieza.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarPiezaActionPerformed(evt);
            }
        });
        jPanel6.add(btnCancelarPieza);
        btnCancelarPieza.setBounds(380, 430, 90, 40);

        btnEditarPieza.setBackground(new java.awt.Color(255, 255, 255));
        btnEditarPieza.setForeground(new java.awt.Color(240, 0, 0));
        btnEditarPieza.setText("Editar");
        btnEditarPieza.setEnabled(false);
        btnEditarPieza.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarPiezaActionPerformed(evt);
            }
        });
        jPanel6.add(btnEditarPieza);
        btnEditarPieza.setBounds(480, 430, 100, 40);

        btnRemoverPieza.setBackground(new java.awt.Color(255, 255, 255));
        btnRemoverPieza.setForeground(new java.awt.Color(240, 0, 0));
        btnRemoverPieza.setText("Remover");
        btnRemoverPieza.setEnabled(false);
        btnRemoverPieza.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverPiezaActionPerformed(evt);
            }
        });
        jPanel6.add(btnRemoverPieza);
        btnRemoverPieza.setBounds(590, 430, 90, 40);

        lblIDBuscarPieza.setText("Ingrese pieza a Buscar:");
        jPanel6.add(lblIDBuscarPieza);
        lblIDBuscarPieza.setBounds(260, 60, 150, 16);
        jPanel6.add(txtDatosPZ);
        txtDatosPZ.setBounds(420, 50, 190, 30);

        lblIDPieza.setText("Código de Barras:");
        jPanel6.add(lblIDPieza);
        lblIDPieza.setBounds(90, 180, 110, 16);

        txtIDPZ.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtIDPZ.setEnabled(false);
        jPanel6.add(txtIDPZ);
        txtIDPZ.setBounds(220, 170, 190, 30);

        lblDescripcionPieza.setText("Descripción:");
        jPanel6.add(lblDescripcionPieza);
        lblDescripcionPieza.setBounds(140, 290, 70, 16);

        txtDescripcionPZ.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtDescripcionPZ.setEnabled(false);
        jPanel6.add(txtDescripcionPZ);
        txtDescripcionPZ.setBounds(220, 280, 230, 30);

        lblStockPieza.setText("Stock:");
        jPanel6.add(lblStockPieza);
        lblStockPieza.setBounds(170, 350, 40, 16);

        txtStockPZ.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtStockPZ.setEnabled(false);
        jPanel6.add(txtStockPZ);
        txtStockPZ.setBounds(220, 340, 150, 30);

        btnBuscarPiezas.setBackground(new java.awt.Color(255, 255, 255));
        btnBuscarPiezas.setForeground(new java.awt.Color(240, 0, 0));
        btnBuscarPiezas.setText("Buscar");
        btnBuscarPiezas.setActionCommand("");
        btnBuscarPiezas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarPiezasActionPerformed(evt);
            }
        });
        jPanel6.add(btnBuscarPiezas);
        btnBuscarPiezas.setBounds(630, 50, 80, 30);

        lblNombrePZ.setText("Nombre:");
        jPanel6.add(lblNombrePZ);
        lblNombrePZ.setBounds(150, 240, 50, 16);

        txtNombrePZ.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtNombrePZ.setEnabled(false);
        jPanel6.add(txtNombrePZ);
        txtNombrePZ.setBounds(220, 230, 190, 28);

        lblFondoUS.setText(" ");
        jPanel6.add(lblFondoUS);
        lblFondoUS.setBounds(746, 253, 4, 16);

        lbl.setText("(Código de barras)");
        jPanel6.add(lbl);
        lbl.setBounds(280, 100, 120, 16);

        lblPrecioPZ.setText("Precio por pieza:");
        jPanel6.add(lblPrecioPZ);
        lblPrecioPZ.setBounds(410, 350, 100, 16);

        txtPrecioPZ.setEnabled(false);
        jPanel6.add(txtPrecioPZ);
        txtPrecioPZ.setBounds(520, 340, 130, 30);

        jTabbedPane1.addTab("Piezas", jPanel6);

        jPanel4.setBackground(new java.awt.Color(234, 234, 234));
        jPanel4.setLayout(null);

        lblIDVehiculoReparaciones.setText("Vehiculo matrícula:");
        jPanel4.add(lblIDVehiculoReparaciones);
        lblIDVehiculoReparaciones.setBounds(29, 140, 110, 16);

        cbVehiculoRP.setEnabled(false);
        jPanel4.add(cbVehiculoRP);
        cbVehiculoRP.setBounds(150, 130, 130, 30);

        txtIDRP.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtIDRP.setEnabled(false);
        jPanel4.add(txtIDRP);
        txtIDRP.setBounds(120, 210, 93, 30);

        btnNuevoRP.setBackground(new java.awt.Color(255, 255, 255));
        btnNuevoRP.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        btnNuevoRP.setForeground(new java.awt.Color(240, 0, 0));
        btnNuevoRP.setText("Nuevo");
        btnNuevoRP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoRPActionPerformed(evt);
            }
        });
        jPanel4.add(btnNuevoRP);
        btnNuevoRP.setBounds(180, 420, 100, 40);

        btnSalvarRP.setBackground(new java.awt.Color(255, 255, 255));
        btnSalvarRP.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        btnSalvarRP.setForeground(new java.awt.Color(240, 0, 0));
        btnSalvarRP.setText("Salvar");
        btnSalvarRP.setEnabled(false);
        btnSalvarRP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarRPActionPerformed(evt);
            }
        });
        jPanel4.add(btnSalvarRP);
        btnSalvarRP.setBounds(290, 420, 90, 40);

        btnCancelarRP.setBackground(new java.awt.Color(255, 255, 255));
        btnCancelarRP.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        btnCancelarRP.setForeground(new java.awt.Color(240, 0, 0));
        btnCancelarRP.setText("Cancelar");
        btnCancelarRP.setEnabled(false);
        btnCancelarRP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarRPActionPerformed(evt);
            }
        });
        jPanel4.add(btnCancelarRP);
        btnCancelarRP.setBounds(390, 420, 90, 40);

        btnEditarRP.setBackground(new java.awt.Color(255, 255, 255));
        btnEditarRP.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        btnEditarRP.setForeground(new java.awt.Color(240, 0, 0));
        btnEditarRP.setText("Editar");
        btnEditarRP.setEnabled(false);
        btnEditarRP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarRPActionPerformed(evt);
            }
        });
        jPanel4.add(btnEditarRP);
        btnEditarRP.setBounds(490, 420, 100, 40);

        btnRemoverRP.setBackground(new java.awt.Color(255, 255, 255));
        btnRemoverRP.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        btnRemoverRP.setForeground(new java.awt.Color(240, 0, 0));
        btnRemoverRP.setText("Remover");
        btnRemoverRP.setEnabled(false);
        btnRemoverRP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverRPActionPerformed(evt);
            }
        });
        jPanel4.add(btnRemoverRP);
        btnRemoverRP.setBounds(600, 420, 90, 40);

        jLabel23.setText("Ingrese reparación a Buscar:");
        jPanel4.add(jLabel23);
        jLabel23.setBounds(286, 37, 180, 16);
        jPanel4.add(txtDatosRP);
        txtDatosRP.setBounds(471, 30, 190, 30);

        lblReparacionID.setText("Reparacion ID:");
        jPanel4.add(lblReparacionID);
        lblReparacionID.setBounds(20, 220, 85, 16);

        txtFechaENRP.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtFechaENRP.setEnabled(false);
        jPanel4.add(txtFechaENRP);
        txtFechaENRP.setBounds(640, 200, 130, 30);

        lblFechaEntradaReparaciones.setText("Fecha Entrada:");
        jPanel4.add(lblFechaEntradaReparaciones);
        lblFechaEntradaReparaciones.setBounds(530, 210, 87, 16);

        lblFechaSalidaReparaciones.setText("Fecha Salida:");
        jPanel4.add(lblFechaSalidaReparaciones);
        lblFechaSalidaReparaciones.setBounds(540, 260, 78, 16);

        txtFechaSARP.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtFechaSARP.setEnabled(false);
        jPanel4.add(txtFechaSARP);
        txtFechaSARP.setBounds(640, 260, 129, 30);

        lblPiezaIDreaparaciones.setText("Pieza código:");
        jPanel4.add(lblPiezaIDreaparaciones);
        lblPiezaIDreaparaciones.setBounds(330, 140, 80, 16);

        lblFallaReparaciones.setText("Falla:");
        jPanel4.add(lblFallaReparaciones);
        lblFallaReparaciones.setBounds(70, 270, 32, 16);

        txtFallaRP.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtFallaRP.setEnabled(false);
        jPanel4.add(txtFallaRP);
        txtFallaRP.setBounds(120, 260, 93, 30);

        btnBuscarReparaciones.setBackground(new java.awt.Color(255, 255, 255));
        btnBuscarReparaciones.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        btnBuscarReparaciones.setForeground(new java.awt.Color(240, 0, 0));
        btnBuscarReparaciones.setText("Buscar");
        btnBuscarReparaciones.setActionCommand("");
        btnBuscarReparaciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarReparacionesActionPerformed(evt);
            }
        });
        jPanel4.add(btnBuscarReparaciones);
        btnBuscarReparaciones.setBounds(668, 30, 73, 30);

        lblCantidadPiezasReparaciones.setText("Cantidad de Piezas:");
        jPanel4.add(lblCantidadPiezasReparaciones);
        lblCantidadPiezasReparaciones.setBounds(235, 210, 120, 16);

        cbPiezaRP.setEnabled(false);
        jPanel4.add(cbPiezaRP);
        cbPiezaRP.setBounds(420, 130, 160, 30);

        lblNOMBREOIDRP.setText("(ID)");
        jPanel4.add(lblNOMBREOIDRP);
        lblNOMBREOIDRP.setBounds(360, 70, 40, 16);

        lblMonto1.setText("Monto piezas utilizadas:");
        jPanel4.add(lblMonto1);
        lblMonto1.setBounds(220, 270, 140, 16);

        lblMonto2.setText("Monto por servicio:");
        jPanel4.add(lblMonto2);
        lblMonto2.setBounds(250, 330, 110, 16);

        txtMontoServicio.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtMontoServicio.setEnabled(false);
        jPanel4.add(txtMontoServicio);
        txtMontoServicio.setBounds(370, 320, 130, 30);

        jLabel2.setText("a usar");
        jPanel4.add(jLabel2);
        jLabel2.setBounds(280, 230, 41, 16);

        txtMontoPZ.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtMontoPZ.setEnabled(false);
        jPanel4.add(txtMontoPZ);
        txtMontoPZ.setBounds(370, 260, 130, 30);

        txtCantidadPiezasRP.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtCantidadPiezasRP.setEnabled(false);
        txtCantidadPiezasRP.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCantidadPiezasRPKeyReleased(evt);
            }
        });
        jPanel4.add(txtCantidadPiezasRP);
        txtCantidadPiezasRP.setBounds(367, 204, 130, 30);

        lblEstatusRP.setText("Estatus pago:");
        jPanel4.add(lblEstatusRP);
        lblEstatusRP.setBounds(540, 330, 90, 16);

        txtEstatusRP.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtEstatusRP.setEnabled(false);
        jPanel4.add(txtEstatusRP);
        txtEstatusRP.setBounds(640, 320, 130, 30);

        lblTipoServicio.setText("Tipo servicio:");
        jPanel4.add(lblTipoServicio);
        lblTipoServicio.setBounds(30, 380, 80, 16);

        cbTipoServicioRP.setEnabled(false);
        jPanel4.add(cbTipoServicioRP);
        cbTipoServicioRP.setBounds(120, 370, 290, 30);

        jTabbedPane1.addTab("Reparaciones", jPanel4);

        jPanel5.setBackground(new java.awt.Color(234, 234, 234));
        jPanel5.setLayout(null);

        lblIDVT.setText("ID Cobro:");
        jPanel5.add(lblIDVT);
        lblIDVT.setBounds(120, 170, 60, 16);

        lblReparacionIDVT.setText("ID Reparación:");
        jPanel5.add(lblReparacionIDVT);
        lblReparacionIDVT.setBounds(90, 120, 85, 16);

        lblTotal.setText("Monto total:");
        jPanel5.add(lblTotal);
        lblTotal.setBounds(110, 350, 80, 16);

        lblTipoPago.setText("Tipo de pago:");
        jPanel5.add(lblTipoPago);
        lblTipoPago.setBounds(370, 350, 90, 16);

        cbIDRPVT.setEnabled(false);
        cbIDRPVT.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbIDRPVTItemStateChanged(evt);
            }
        });
        jPanel5.add(cbIDRPVT);
        cbIDRPVT.setBounds(200, 110, 130, 30);

        txtIDVT.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtIDVT.setEnabled(false);
        jPanel5.add(txtIDVT);
        txtIDVT.setBounds(200, 160, 100, 30);

        txtTotalVT.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtTotalVT.setEnabled(false);
        jPanel5.add(txtTotalVT);
        txtTotalVT.setBounds(200, 340, 130, 30);

        cbTipoPagoVT.setEnabled(false);
        jPanel5.add(cbTipoPagoVT);
        cbTipoPagoVT.setBounds(470, 340, 130, 30);

        btnNuevoVT.setBackground(new java.awt.Color(255, 255, 255));
        btnNuevoVT.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        btnNuevoVT.setForeground(new java.awt.Color(240, 0, 0));
        btnNuevoVT.setText("Nuevo");
        btnNuevoVT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoVTActionPerformed(evt);
            }
        });
        jPanel5.add(btnNuevoVT);
        btnNuevoVT.setBounds(210, 420, 100, 40);

        btnSalvarVT.setBackground(new java.awt.Color(255, 255, 255));
        btnSalvarVT.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        btnSalvarVT.setForeground(new java.awt.Color(240, 0, 0));
        btnSalvarVT.setText("Salvar");
        btnSalvarVT.setEnabled(false);
        btnSalvarVT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarVTActionPerformed(evt);
            }
        });
        jPanel5.add(btnSalvarVT);
        btnSalvarVT.setBounds(320, 420, 90, 40);

        btnCancelarVT.setBackground(new java.awt.Color(255, 255, 255));
        btnCancelarVT.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        btnCancelarVT.setForeground(new java.awt.Color(240, 0, 0));
        btnCancelarVT.setText("Cancelar");
        btnCancelarVT.setEnabled(false);
        btnCancelarVT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarVTActionPerformed(evt);
            }
        });
        jPanel5.add(btnCancelarVT);
        btnCancelarVT.setBounds(420, 420, 90, 40);

        btnEditarVT.setBackground(new java.awt.Color(255, 255, 255));
        btnEditarVT.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        btnEditarVT.setForeground(new java.awt.Color(240, 0, 0));
        btnEditarVT.setText("Editar");
        btnEditarVT.setEnabled(false);
        btnEditarVT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarVTActionPerformed(evt);
            }
        });
        jPanel5.add(btnEditarVT);
        btnEditarVT.setBounds(520, 420, 100, 40);

        btnRemoverVT.setBackground(new java.awt.Color(255, 255, 255));
        btnRemoverVT.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        btnRemoverVT.setForeground(new java.awt.Color(240, 0, 0));
        btnRemoverVT.setText("Remover");
        btnRemoverVT.setEnabled(false);
        btnRemoverVT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverVTActionPerformed(evt);
            }
        });
        jPanel5.add(btnRemoverVT);
        btnRemoverVT.setBounds(630, 420, 90, 40);

        btnBuscarVT.setBackground(new java.awt.Color(255, 255, 255));
        btnBuscarVT.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        btnBuscarVT.setForeground(new java.awt.Color(240, 0, 0));
        btnBuscarVT.setText("Buscar");
        btnBuscarVT.setActionCommand("");
        btnBuscarVT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarVTActionPerformed(evt);
            }
        });
        jPanel5.add(btnBuscarVT);
        btnBuscarVT.setBounds(668, 30, 73, 30);

        lblDatosVT.setText("Ingrese cobro a buscar:");
        jPanel5.add(lblDatosVT);
        lblDatosVT.setBounds(280, 40, 140, 16);
        jPanel5.add(txtDatosVT);
        txtDatosVT.setBounds(430, 30, 220, 30);

        jLabel3.setText("(ID)");
        jPanel5.add(jLabel3);
        jLabel3.setBounds(343, 67, 31, 16);

        lblFechaVT.setText("Fecha de cobro:");
        jPanel5.add(lblFechaVT);
        lblFechaVT.setBounds(90, 230, 100, 16);

        txtFechaCobroVT.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtFechaCobroVT.setEnabled(false);
        jPanel5.add(txtFechaCobroVT);
        txtFechaCobroVT.setBounds(200, 220, 130, 30);

        lblFechaPago.setText("Fecha de pago:");
        jPanel5.add(lblFechaPago);
        lblFechaPago.setBounds(90, 290, 90, 16);

        txtFechaPagoVT.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtFechaPagoVT.setEnabled(false);
        jPanel5.add(txtFechaPagoVT);
        txtFechaPagoVT.setBounds(200, 280, 130, 30);

        lblClienteVT.setText("Cliente:");
        jPanel5.add(lblClienteVT);
        lblClienteVT.setBounds(360, 170, 50, 16);

        txtClienteVT.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtClienteVT.setEnabled(false);
        jPanel5.add(txtClienteVT);
        txtClienteVT.setBounds(420, 160, 170, 30);

        lblEstatusCB.setText("Estatus pago:");
        jPanel5.add(lblEstatusCB);
        lblEstatusCB.setBounds(370, 290, 80, 16);

        txtEstatusCB.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtEstatusCB.setEnabled(false);
        jPanel5.add(txtEstatusCB);
        txtEstatusCB.setBounds(470, 280, 130, 30);

        jTabbedPane1.addTab("Cobro", jPanel5);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 920, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 566, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //Pestaña Usuario   
    private void btnNuevoUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoUsuarioActionPerformed
        HabilitarTextFieldUsuarios();
        LimpiarUsuarios();
        btnNuevoUsuario.setEnabled(false);
        btnSalvarUsuario.setEnabled(true);
        btnCancelarUsuario.setEnabled(true);
        btnEditarUsuario.setEnabled(false);
        btnRemoverUsuario.setEnabled(false);

        txtNombreUS.setEditable(true);
        txtAPUS.setEditable(true);
        txtAMUS.setEditable(true);
        txtTelefonoUS.setEditable(true);
        txtDireccionUS.setEditable(true);
        txtIDUS.setEnabled(false);
        txtIDUS.setEditable(false);
        
        int ID = 0;
        
        try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String sql="SELECT * FROM usuarios";
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                ID = Integer.valueOf(rs.getString(1));
            }
            
            ++ID;
            txtIDUS.setText(String.valueOf(ID));
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,"Hubo un error con los ID auto incrementales");
        }
        
    }//GEN-LAST:event_btnNuevoUsuarioActionPerformed

    private void btnSalvarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarUsuarioActionPerformed
        boolean ban = false;
        boolean valid = false;
        if("".equals(txtDatosUS.getText())){                 
            if(ban == false){
                if(ValidarP(txtNombreUS.getText())){
                    valid = true;
                    txtNombreUS.setText("");
                }else if(ValidarP(txtAPUS.getText())){
                    valid = true;
                    txtAPUS.setText("");
                }else if(ValidarP(txtAMUS.getText())){
                    valid = true;
                    txtAMUS.setText("");
                }else{
                    try{
                        int telefo = Integer.parseInt(txtTelefonoUS.getText());
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(this,"El telefono debe ser numerico");
                        valid = true;
                        txtTelefonoUS.setText("");
                    }
                }
                if(valid == false){
                    Conexion cc=new Conexion();
                    Connection cn=cc.conexion();
                    try{
                        PreparedStatement pst=cn.prepareStatement("INSERT INTO usuarios(ID_US,NOMBRE_US,AP_US,AM_US,TELEFONO_US,DIRECCION_US) VALUES(?,?,?,?,?,?)");
                        pst.setInt(1,Integer.valueOf(txtIDUS.getText()));
                        pst.setString(2,txtNombreUS.getText());
                        pst.setString(3,txtAPUS.getText());
                        pst.setString(4,txtAMUS.getText());
                        pst.setInt(5,Integer.valueOf(txtTelefonoUS.getText()));
                        pst.setString(6,txtDireccionUS.getText());

                        int a=pst.executeUpdate();

                        if(a>0){
                            NuevoTrueUsuarios();
                            LimpiarUsuarios();
                            DesHabilitarTextFieldUsuarios();
                            JOptionPane.showMessageDialog(null,"Registro exitoso");
                        }else{
                            NuevoTrueUsuarios();
                            LimpiarUsuarios();
                            DesHabilitarTextFieldUsuarios();
                            JOptionPane.showMessageDialog(null,"Error al agregar");
                        }
                    }catch(Exception e){

                    }
                }
            }
        }else if(!"".equals(txtDatosUS.getText()) && !"".equals(txtNombreUS.getText())){
            if(ValidarP(txtNombreUS.getText())){
                valid = true;
                txtNombreUS.setText("");
            }else if(ValidarP(txtAPUS.getText())){
                valid = true;
                txtAPUS.setText("");
            }else if(ValidarP(txtAMUS.getText())){
                valid = true;
                txtAMUS.setText("");
            }else{
                try{
                    int telefo = Integer.parseInt(txtTelefonoUS.getText());
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(this,"El telefono debe ser numerico");
                    valid = true;
                    txtTelefonoUS.setText("");
                }
            }
            if(valid == false){
                Conexion cc=new Conexion();
                Connection cn=cc.conexion();
                String valor = txtDatosUS.getText();
                try{
                    PreparedStatement pst=cn.prepareStatement("UPDATE usuarios SET ID_US='"+Integer.valueOf(txtIDUS.getText())+"',NOMBRE_US='"+txtNombreUS.getText()+"',AP_US='"+txtAPUS.getText()+
                    "',AM_US='"+txtAMUS.getText()+"',TELEFONO_US='"+Integer.valueOf(txtTelefonoUS.getText())+"',DIRECCION_US='"+txtDireccionUS.getText()+"' WHERE (ID_US='"+valor+"'OR NOMBRE_US='"+valor+"')");
                    pst.executeUpdate();

                    NuevoTrueUsuarios();
                    LimpiarUsuarios();
                    DesHabilitarTextFieldUsuarios();
                    JOptionPane.showMessageDialog(null,"Actualización exitosa");
                }catch(Exception e){
                    JOptionPane.showMessageDialog(null,"Hubo un error con la actualización");
                }
            }

        }
    }//GEN-LAST:event_btnSalvarUsuarioActionPerformed

    private void btnCancelarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarUsuarioActionPerformed
        DesHabilitarTextFieldUsuarios();
        LimpiarUsuarios();
        NuevoTrueUsuarios();
    }//GEN-LAST:event_btnCancelarUsuarioActionPerformed

    private void btnEditarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarUsuarioActionPerformed
        HabilitarTextFieldUsuarios();

        btnNuevoUsuario.setEnabled(false);
        btnSalvarUsuario.setEnabled(true);
        btnCancelarUsuario.setEnabled(true);
        btnEditarUsuario.setEnabled(false);
        btnRemoverUsuario.setEnabled(false);

        txtNombreUS.setEditable(true);
        txtAPUS.setEditable(true);
        txtAMUS.setEditable(true);
        txtTelefonoUS.setEditable(true);
        txtDireccionUS.setEditable(true);
    }//GEN-LAST:event_btnEditarUsuarioActionPerformed

    private void btnRemoverUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverUsuarioActionPerformed
        boolean ban = false;
        Conexion cc=new Conexion();
        Connection cn=cc.conexion();
        String valor = txtDatosUS.getText();
        PreparedStatement pst;
        try {
            pst = cn.prepareStatement("DELETE FROM usuarios WHERE (ID_US='"+valor+"'OR NOMBRE_US='"+valor+"')");
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null,"Se eliminó el registro con éxito");
            ban = true;
        } catch (SQLException ex) {
             JOptionPane.showMessageDialog(null,"Hubo un error con la eliminación del registro");
        }
        
        if(ban == true){
            LimpiarUsuarios();
            DesHabilitarTextFieldUsuarios();
            NuevoTrueUsuarios();
            txtIDUS.setEnabled(false);
        }
    }//GEN-LAST:event_btnRemoverUsuarioActionPerformed

    private void btnBuscarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarUsuarioActionPerformed
        try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String valor = txtDatosUS.getText();
            String sql="";
            sql="SELECT * FROM usuarios WHERE (ID_US='"+valor+"'  OR NOMBRE_US='"+valor+"')";
            
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                txtIDUS.setText(String.valueOf(rs.getInt(1)));
                txtNombreUS.setText(rs.getString(2));
                txtAPUS.setText(rs.getString(3));
                txtAMUS.setText(rs.getString(4));
                txtTelefonoUS.setText(String.valueOf(rs.getInt(5)));
                txtDireccionUS.setText(rs.getString(6));
            }
            if("".equals(txtIDUS.getText())){
                JOptionPane.showMessageDialog(this,"No se encontró el registro a buscar");
                NuevoTrueUsuarios();
                LimpiarUsuarios();
                DesHabilitarTextFieldUsuarios();
            }else{
                btnNuevoUsuario.setEnabled(false);
                btnSalvarUsuario.setEnabled(false);
                btnCancelarUsuario.setEnabled(false);
                btnEditarUsuario.setEnabled(true);
                btnRemoverUsuario.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,"Hubo un error con la búsqueda");
        }
    }//GEN-LAST:event_btnBuscarUsuarioActionPerformed

    //Pestaña Cliente
    
    private void btnNuevoClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoClienteActionPerformed
        LimpiarClientes();
        HabilitarTextFieldClientes();
        btnNuevoCliente.setEnabled(false);
        btnSalvarCliente.setEnabled(true);
        btnCancelarCliente.setEnabled(true);
        btnEditarCliente.setEnabled(false);
        btnRemoverCliente.setEnabled(false);

        txtCURPCT.setEnabled(true);
        txtCURPCT.setEditable(true);
        txtFechaCT.setText(GenerarFecha());

    }//GEN-LAST:event_btnNuevoClienteActionPerformed

    private void btnSalvarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarClienteActionPerformed
        boolean ban = false;
        boolean valid = false;
        if("".equals(txtDatosCT.getText())){          
            if(cbIDUsuarioCT.getItemAt(cbIDUsuarioCT.getSelectedIndex()) == null){
                JOptionPane.showMessageDialog(null, "Se necesita de un Usuario válido para agregar un cliente");
                DesHabilitarTextFieldClientes();
                LimpiarClientes();
                NuevoTrueClientes();
            }else{
                if(ban == false){
                    if(ValidarP(txtNombreCT.getText())){
                        valid = true;
                        txtNombreCT.setText("");
                    }else if(ValidarP(txtAPCT.getText())){
                        valid = true;
                        txtAPCT.setText("");
                    }else if(ValidarP(txtAMCT.getText())){
                        valid = true;
                        txtAMCT.setText("");
                    }
                    if(valid == false){
                        Conexion cc=new Conexion();
                        Connection cn=cc.conexion();
                        try{
                            PreparedStatement pst=cn.prepareStatement("INSERT INTO clientes(ID_USCT,CURP_CT,NOMBRE_CT,AP_CT,AM_CT,DIRECCION_CT,FECHA_CT) VALUES(?,?,?,?,?,?,?)");
                            pst.setInt(1, Integer.valueOf(cbIDUsuarioCT.getSelectedItem().toString()));
                            pst.setString(2,txtCURPCT.getText());
                            pst.setString(3,txtNombreCT.getText());
                            pst.setString(4,txtAPCT.getText());
                            pst.setString(5,txtAMCT.getText());
                            pst.setString(6, txtDireccionCT.getText());
                            pst.setString(7, txtFechaCT.getText());

                            int a=pst.executeUpdate();

                            if(a>0){
                                DesHabilitarTextFieldClientes();
                                LimpiarClientes();
                                NuevoTrueClientes();
                                JOptionPane.showMessageDialog(null,"Registro exitoso");
                            }else{
                                DesHabilitarTextFieldClientes();
                                LimpiarClientes();
                                NuevoTrueClientes();
                                JOptionPane.showMessageDialog(null,"Error al agregar");
                            }
                        }catch(Exception e){

                        }
                    }
                }
            }

        }else if(!"".equals(txtDatosCT.getText()) && !"".equals(txtNombreCT.getText())){
            if(cbIDUsuarioCT.getItemAt(cbIDUsuarioCT.getSelectedIndex()) == null){
                JOptionPane.showMessageDialog(null, "Se necesita de un Usuario válido para agregar un cliente");
                DesHabilitarTextFieldClientes();
                LimpiarClientes();
                NuevoTrueClientes();
            }else{
                if(ValidarP(txtNombreCT.getText())){
                    valid = true;
                    txtNombreCT.setText("");
                }else if(ValidarP(txtAPCT.getText())){
                    valid = true;
                    txtAPCT.setText("");
                }else if(ValidarP(txtAMCT.getText())){
                    valid = true;
                    txtAMCT.setText("");
                }
                if(valid == false){
                    Conexion cc=new Conexion();
                    Connection cn=cc.conexion();
                    String valor = txtDatosCT.getText();
                    try{
                        PreparedStatement pst=cn.prepareStatement("UPDATE clientes SET ID_USCT='"+Integer.valueOf(cbIDUsuarioCT.getSelectedItem().toString())+"',CURP_CT='"+
                        txtCURPCT.getText()+"',NOMBRE_CT='"+txtNombreCT.getText()+"',AP_CT='"+txtAPCT.getText()+"',AM_CT='"+txtAMCT.getText()+"',DIRECCION_CT='"+
                        txtDireccionCT.getText()+"',FECHA_CT='"+txtFechaCT.getText()+"' WHERE (CURP_CT='"+valor+"')");
                        pst.executeUpdate();
                        DesHabilitarTextFieldClientes();
                        LimpiarClientes();
                        NuevoTrueClientes();
                        JOptionPane.showMessageDialog(null,"Actualización exitosa");
                    }catch(Exception e){
                        JOptionPane.showMessageDialog(null,"Hubo un error con la actualización");
                        DesHabilitarTextFieldClientes();
                        LimpiarClientes();
                        NuevoTrueClientes();
                    }
                }
            }
        }      
    }//GEN-LAST:event_btnSalvarClienteActionPerformed

    private void btnCancelarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarClienteActionPerformed
        DesHabilitarTextFieldClientes();
        LimpiarClientes();
        NuevoTrueClientes();
    }//GEN-LAST:event_btnCancelarClienteActionPerformed

    private void btnEditarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarClienteActionPerformed
        HabilitarTextFieldClientes();
        btnNuevoCliente.setEnabled(false);
        btnSalvarCliente.setEnabled(true);
        btnCancelarCliente.setEnabled(true);
        btnEditarCliente.setEnabled(false);
        btnRemoverCliente.setEnabled(false);
        cbIDUsuarioCT.setEnabled(false);

        txtNombreCT.setEditable(true);
        txtAPCT.setEditable(true);
        txtAMCT.setEditable(true);
        txtDireccionCT.setEditable(true);
    }//GEN-LAST:event_btnEditarClienteActionPerformed

    private void btnRemoverClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverClienteActionPerformed
        Conexion cc=new Conexion();
        Connection cn=cc.conexion();
        String valor = txtDatosCT.getText();
        PreparedStatement pst;
        try {
            pst = cn.prepareStatement("DELETE FROM clientes WHERE (CURP_CT='"+valor+"')");
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null,"Se eliminó el registro con éxito");
        } catch (SQLException ex) {
             JOptionPane.showMessageDialog(null,"Hubo un error con la eliminación del registro");
        }

        LimpiarClientes();
        DesHabilitarTextFieldClientes();
        NuevoTrueClientes();
        txtCURPCT.setEnabled(false);
    }//GEN-LAST:event_btnRemoverClienteActionPerformed

    private void btnBuscarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarClienteActionPerformed
        cbIDUsuarioCT.setSelectedIndex(-1);
        try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String valor = txtDatosCT.getText();
            String sql="";
            sql="SELECT * FROM clientes WHERE (CURP_CT='"+valor+"')";
            
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                cbIDUsuarioCT.setSelectedItem(rs.getString(1));
                txtCURPCT.setText(rs.getString(2));
                txtNombreCT.setText(rs.getString(3));
                txtAPCT.setText(rs.getString(4));
                txtAMCT.setText(rs.getString(5));
                txtDireccionCT.setText(rs.getString(6));
                txtFechaCT.setText(rs.getString(7));
            }
            if(cbIDUsuarioCT.getItemAt(cbIDUsuarioCT.getSelectedIndex()) == null){
                JOptionPane.showMessageDialog(null,"No se encontró el registro a buscar");
                DesHabilitarTextFieldClientes();
                LimpiarClientes();
                NuevoTrueClientes();
            }else{
                btnNuevoCliente.setEnabled(false);
                btnSalvarCliente.setEnabled(false);
                btnCancelarCliente.setEnabled(false);
                btnEditarCliente.setEnabled(true);
                btnRemoverCliente.setEnabled(true);
            }        
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,"Hubo un error con la búsqueda");
        }
    }//GEN-LAST:event_btnBuscarClienteActionPerformed

    //Pestaña Vehiculo
    
    private void btnNuevoVehiculoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoVehiculoActionPerformed
        LimpiarVehiculos();
        HabilitarTextFieldVehiculos();
        btnNuevoVehiculo.setEnabled(false);
        btnSalvarVehiculo.setEnabled(true);
        btnCancelarVehiculo.setEnabled(true);
        btnEditarVehiculo.setEnabled(false);
        btnRemoverVehiculo.setEnabled(false);

        txtMatriculaVH.setEnabled(true);
        txtMarcaVH.setEnabled(true);
        txtModeloVH.setEnabled(true);
        txtFechaVH.setText(GenerarFecha());  
    }//GEN-LAST:event_btnNuevoVehiculoActionPerformed

    private void btnSalvarVehiculoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarVehiculoActionPerformed
        boolean ban = false;
        boolean valid = false;
        if("".equals(txtDatosVH.getText())){          
            if(cbIDClienteVH.getItemAt(cbIDClienteVH.getSelectedIndex()) == null){
                JOptionPane.showMessageDialog(null, "Se necesita de un Cliente válido para agregar un cliente");
                DesHabilitarTextFieldVehiculos();
                LimpiarVehiculos();
                NuevoTrueVehiculo();
                valid = true;
            }else{
                if(ban == false){
                    if(ValidarP(txtMarcaVH.getText())){
                        valid = true;
                        txtMarcaVH.setText("");
                    }
                    if(valid == false){
                        Conexion cc=new Conexion();
                        Connection cn=cc.conexion();
                        try{
                            PreparedStatement pst=cn.prepareStatement("INSERT INTO vehiculos(ID_CTVH,MATRICULA_VH,MARCA_VH,MODELO_VH,FECHA_VH) VALUES(?,?,?,?,?)");
                            pst.setString(1, cbIDClienteVH.getSelectedItem().toString());
                            pst.setString(2,txtMatriculaVH.getText());
                            pst.setString(3,txtMarcaVH.getText());
                            pst.setString(4,txtModeloVH.getText());
                            pst.setString(5, txtFechaVH.getText());
                            int a=pst.executeUpdate();
                            if(a>0){
                                DesHabilitarTextFieldVehiculos();
                                LimpiarVehiculos();
                                NuevoTrueVehiculo();
                                JOptionPane.showMessageDialog(null,"Registro exitoso");
                            }else{
                                DesHabilitarTextFieldVehiculos();
                                LimpiarVehiculos();
                                NuevoTrueVehiculo();
                                JOptionPane.showMessageDialog(null,"Error al agregar");
                            }
                        }catch(Exception e){
                        }
                    }
                }
            }
            
        }else if(!"".equals(txtDatosVH.getText()) && !"".equals(txtMarcaVH.getText())){
            if(cbIDClienteVH.getItemAt(cbIDClienteVH.getSelectedIndex()) == null){
                JOptionPane.showMessageDialog(null, "Se necesita de un Usuario válido para agregar un cliente");
                DesHabilitarTextFieldVehiculos();
                LimpiarVehiculos();
                NuevoTrueVehiculo();
            }else{
                if(ValidarP(txtMarcaVH.getText())){
                    valid = true;
                    txtMarcaVH.setText("");
                }
                if(valid == false){
                    Conexion cc=new Conexion();
                    Connection cn=cc.conexion();
                    String valor = txtDatosVH.getText();
                    try{
                        PreparedStatement pst=cn.prepareStatement("UPDATE vehiculos SET ID_CTVH='"+cbIDClienteVH.getSelectedItem().toString()
                        +"',MATRICULA_VH='"+txtMatriculaVH.getText()+"',MARCA_VH='"+txtMarcaVH.getText()+"',MODELO_VH='"+txtModeloVH.getText()+"',FECHA_VH='"+
                        txtFechaVH.getText()+"' WHERE (MATRICULA_VH='"+valor+"')");
                        pst.executeUpdate();

                        DesHabilitarTextFieldVehiculos();
                        LimpiarVehiculos();
                        NuevoTrueVehiculo();
                        JOptionPane.showMessageDialog(null,"Actualización exitosa");
                    }catch(Exception e){
                        JOptionPane.showMessageDialog(null,"Hubo un error con la actualización");
                        DesHabilitarTextFieldVehiculos();
                        LimpiarVehiculos();
                        NuevoTrueVehiculo();
                    }
                }
            }
        }  
        
    }//GEN-LAST:event_btnSalvarVehiculoActionPerformed

    private void btnCancelarVehiculoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarVehiculoActionPerformed
        DesHabilitarTextFieldVehiculos();
        LimpiarVehiculos();
        NuevoTrueVehiculo();
    }//GEN-LAST:event_btnCancelarVehiculoActionPerformed

    private void btnEditarVehiculoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarVehiculoActionPerformed
        HabilitarTextFieldVehiculos();
        txtMatriculaVH.setEnabled(false);
        btnNuevoVehiculo.setEnabled(false);
        btnSalvarVehiculo.setEnabled(true);
        btnCancelarVehiculo.setEnabled(true);
        btnEditarVehiculo.setEnabled(false);
        btnRemoverVehiculo.setEnabled(false);
        cbIDClienteVH.setEnabled(false);
    }//GEN-LAST:event_btnEditarVehiculoActionPerformed

    private void btnRemoverVehiculoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverVehiculoActionPerformed
        cbVehiculoRP.removeAllItems();
        Conexion cc=new Conexion();
        Connection cn=cc.conexion();
        String valor = txtDatosVH.getText();
        PreparedStatement pst;
        try {
            pst = cn.prepareStatement("DELETE FROM vehiculos WHERE (MATRICULA_VH='"+valor+"')");
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null,"Se eliminó el registro con éxito");
        } catch (SQLException ex) {
             JOptionPane.showMessageDialog(null,"Hubo un error con la eliminación del registro");
        }
        
        try {
            String sql="SELECT * FROM vehiculos";
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                cbVehiculoRP.addItem(rs.getString(2));       
            }    
        } catch (SQLException ex) {
        } 
        LimpiarVehiculos();
        DesHabilitarTextFieldVehiculos();
        NuevoTrueVehiculo();
    }//GEN-LAST:event_btnRemoverVehiculoActionPerformed

    private void btnBuscarVehiculoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarVehiculoActionPerformed
        cbIDClienteVH.setSelectedIndex(-1);
        try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String valor = txtDatosVH.getText();
            String sql="";
            sql="SELECT * FROM vehiculos WHERE (MATRICULA_VH='"+valor+"')";
            
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                cbIDClienteVH.setSelectedItem(rs.getString(1));
                txtMatriculaVH.setText(rs.getString(2));
                txtMarcaVH.setText(rs.getString(3));
                txtModeloVH.setText(rs.getString(4));
                txtFechaVH.setText(rs.getString(5));
            }
            if(cbIDClienteVH.getItemAt(cbIDClienteVH.getSelectedIndex()) == null){
                JOptionPane.showMessageDialog(null,"No se encontró el registro a buscar");
                DesHabilitarTextFieldVehiculos();
                LimpiarVehiculos();
                NuevoTrueVehiculo();
            }else{
                DesHabilitarTextFieldVehiculos();
                btnNuevoVehiculo.setEnabled(false);
                btnSalvarVehiculo.setEnabled(false);
                btnCancelarVehiculo.setEnabled(false);
                btnEditarVehiculo.setEnabled(true);
                btnRemoverVehiculo.setEnabled(true);  
            }        
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,"Hubo un error con la búsqueda");
        }
    }//GEN-LAST:event_btnBuscarVehiculoActionPerformed

    //Pestaña Reparación
    
    private void btnNuevoRPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoRPActionPerformed
        HabilitarTextFieldReparaciones();
        btnNuevoRP.setEnabled(false);
        btnSalvarRP.setEnabled(true);
        btnCancelarRP.setEnabled(true);
        btnEditarRP.setEnabled(false);
        btnRemoverRP.setEnabled(false);
        txtIDRP.setEnabled(false);
        txtIDRP.setEditable(false);int ID = 0;
        try {Conexion cc=new Conexion();Connection cn=cc.conexion();
            String sql="SELECT * FROM reparaciones";
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                ID = Integer.valueOf(rs.getString(3));
            }
            ++ID;
            txtIDRP.setText(String.valueOf(ID));
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,"Hubo un error con los ID auto incrementales");
        }
        txtEstatusRP.setText("NO PAGADO");
        cbTipoServicioRP.addItem("Lubricación y cambio de aceite");
        cbTipoServicioRP.addItem("Reemplazo de acumulador");
        cbTipoServicioRP.addItem("Suspensión");
        cbTipoServicioRP.addItem("Alineación y Balanceo");
        cbTipoServicioRP.addItem("Mofles");
        cbTipoServicioRP.addItem("Rectificación y Torneado");
        cbTipoServicioRP.addItem("Servicio de Clutch");
        cbTipoServicioRP.addItem("Servicio de Diferenciales");
        cbTipoServicioRP.addItem("Engrasado");
        cbTipoServicioRP.addItem("Rotación");
        cbTipoServicioRP.addItem("Revisión, limpieza y ajuste de frenos");
        cbTipoServicioRP.addItem("Otro tipo de reparación");      
    }//GEN-LAST:event_btnNuevoRPActionPerformed

    private void btnSalvarRPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarRPActionPerformed
        boolean ban = false;
        if ("".equals(txtDatosRP.getText())) {
            if(cbVehiculoRP.getItemAt(cbVehiculoRP.getSelectedIndex()) == null){
                JOptionPane.showMessageDialog(null, "Se necesita de un Vehiculo válido para agregar una reparacion");
                DesHabilitarTextFieldReparaciones();
                LimpiarReparacion();
                NuevoTrueReparacion();
            }else if(cbPiezaRP.getItemAt(cbPiezaRP.getSelectedIndex()) == null){
                if(cbPiezaRP.getItemAt(cbPiezaRP.getSelectedIndex()) == null){
                    JOptionPane.showMessageDialog(null, "Se necesita de una Pieza válida para agregar una reparacion");
                    DesHabilitarTextFieldReparaciones();
                    LimpiarReparacion();
                    NuevoTrueReparacion();
                }
            }else{
                if(ValidarP(txtFallaRP.getText())){
                    ban = true;
                    txtFallaRP.setText("");
                }
                try{
                    int piezas = Integer.parseInt(txtCantidadPiezasRP.getText());
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(this,"El campos Cantidad de Piezas debe ser numerico");
                    ban = true;
                    txtCantidadPiezasRP.setText("");
                }
                int stock = 0;
                try{
                    Conexion cc=new Conexion();
                    Connection cn=cc.conexion();
                    String valor = cbPiezaRP.getSelectedItem().toString();
                    String sql="SELECT * FROM piezas WHERE (CBARRAS_PZ='"+valor+"')";

                    Statement st=cn.createStatement();
                    ResultSet rs=st.executeQuery(sql);
                    while(rs.next()){
                        stock = Integer.valueOf(rs.getString(4));
                    }
                                
                    } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(this,"Hubo un error con la búsqueda");
                    }
                if(stock == 0){
                    JOptionPane.showMessageDialog(this,"No hay piezas en stock de la pieza: "+cbPiezaRP.getSelectedItem().toString());
                    ban = true;
                }
                if(Integer.parseInt(txtCantidadPiezasRP.getText()) > stock && ban == false){
                    JOptionPane.showMessageDialog(this,"No hay suficientes piezas en stock de la pieza: "+cbPiezaRP.getSelectedItem().toString());
                    ban = true;
                }
                
                if(ban == false){
                    Conexion cc=new Conexion();
                    Connection cn=cc.conexion();
                    try{
                        PreparedStatement pst=cn.prepareStatement("INSERT INTO reparaciones(VEHICULO_ID_RP, PIEZA_ID_RP, REPARACION_ID_RP, FALLA_RP, CANTIDAD_PZ_RP, MONTO_PZ_RP, MONTO_SERVICIO_RP, FECHA_EN_RP, FECHA_SAL_RP, ESTATUS_RP,TIPO_SERVICIO_RP) VALUES(?,?,?,?,?,?,?,?,?,?,?)");
                        pst.setString(1, cbVehiculoRP.getSelectedItem().toString());
                        pst.setLong(2, Long.valueOf(cbPiezaRP.getSelectedItem().toString()));
                        pst.setInt(3, Integer.valueOf(txtIDRP.getText()));
                        pst.setString(4, txtFallaRP.getText());
                        pst.setInt(5, Integer.valueOf(txtCantidadPiezasRP.getText()));
                        pst.setFloat(6, Float.valueOf(txtMontoPZ.getText()));
                        pst.setFloat(7, Float.valueOf(txtMontoServicio.getText()));
                        pst.setString(8, txtFechaENRP.getText());
                        pst.setString(9, txtFechaSARP.getText());
                        pst.setString(10, txtEstatusRP.getText());
                        pst.setString(11, cbTipoServicioRP.getSelectedItem().toString());
                        
                        int a=pst.executeUpdate();

                        if(a>0){
                            stock = stock - Integer.valueOf(txtCantidadPiezasRP.getText());
                            
                            String valor = cbPiezaRP.getSelectedItem().toString();

                            try{
                                pst=cn.prepareStatement("UPDATE piezas SET STOCK_PZ='"+stock+"' WHERE (CBARRAS_PZ='"+valor+"')");
                                pst.executeUpdate();

                            }catch(Exception e){
                                JOptionPane.showMessageDialog(null,"Hubo un error con la actualización");
                            }
                            
                            DesHabilitarTextFieldReparaciones();
                            LimpiarReparacion();
                            NuevoTrueReparacion();
                            JOptionPane.showMessageDialog(null,"Registro exitoso");
                        }else{
                            DesHabilitarTextFieldReparaciones();
                            LimpiarReparacion();
                            NuevoTrueReparacion();
                            JOptionPane.showMessageDialog(null,"Error al agregar");
                        }
                    }catch(Exception e){

                    }

                }
            }
        }else if(!"".equals(txtDatosRP.getText())){
            if(cbVehiculoRP.getItemAt(cbVehiculoRP.getSelectedIndex()) == null){
                JOptionPane.showMessageDialog(null, "Se necesita de un Vehiculo válido para agregar una reparacion");
                DesHabilitarTextFieldReparaciones();
                LimpiarReparacion();
                NuevoTrueReparacion();
            }else if(cbPiezaRP.getItemAt(cbPiezaRP.getSelectedIndex()) == null){
                if(cbPiezaRP.getItemAt(cbPiezaRP.getSelectedIndex()) == null){
                    JOptionPane.showMessageDialog(null, "Se necesita de una Pieza válida para agregar una reparacion");
                    DesHabilitarTextFieldReparaciones();
                    LimpiarReparacion();
                    NuevoTrueReparacion();
                }
            }else{
                boolean valid = false;
                if(ValidarP(txtFallaRP.getText())){
                    valid = true;
                    txtFallaRP.setText("");
                }
                try{
                    int piezas = Integer.parseInt(txtCantidadPiezasRP.getText());
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(this,"El campos Cantidad de Piezas debe ser numerico");
                    valid = true;
                    txtCantidadPiezasRP.setText("");
                }
                if(valid == false){
                    Conexion cc=new Conexion();
                    Connection cn=cc.conexion();
                    String valor = txtDatosRP.getText();
                    try{
                        PreparedStatement pst=cn.prepareStatement("UPDATE reparaciones SET VEHICULO_ID_RP='"+cbVehiculoRP.getSelectedItem().toString()
                        +"',PIEZA_ID_RP='"+Long.valueOf(cbPiezaRP.getSelectedItem().toString())+"',REPARACION_ID_RP='"+Integer.valueOf(txtIDRP.getText())+"',FALLA_RP='"+txtFallaRP.getText()
                        +"',CANTIDAD_PZ_RP='"+Integer.valueOf(txtCantidadPiezasRP.getText())+"',MONTO_PZ_RP='"+Float.valueOf(txtMontoPZ.getText())+"',MONTO_SERVICIO_RP='"+Float.valueOf(txtMontoServicio.getText())
                        +"',FECHA_EN_RP='"+txtFechaENRP.getText()+"',FECHA_SAL_RP='"+txtFechaSARP.getText()+"',ESTATUS_RP='"+txtEstatusRP.getText()+"',TIPO_SERVICIO_RP='"+cbTipoServicioRP.getSelectedItem().toString()
                        +"' WHERE (REPARACION_ID_RP='"+valor+"')");
                        pst.executeUpdate();
                        DesHabilitarTextFieldReparaciones();
                        LimpiarReparacion();
                        NuevoTrueReparacion();
                        JOptionPane.showMessageDialog(null,"Actualización exitosa");
                    }catch(Exception e){
                        JOptionPane.showMessageDialog(null,"Hubo un error con la actualización");
                        DesHabilitarTextFieldReparaciones();
                        LimpiarReparacion();
                        NuevoTrueReparacion();
                    }
                }     
            }          
        }
    }//GEN-LAST:event_btnSalvarRPActionPerformed

    private void btnCancelarRPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarRPActionPerformed
        DesHabilitarTextFieldReparaciones();
        LimpiarReparacion();
        NuevoTrueReparacion();
    }//GEN-LAST:event_btnCancelarRPActionPerformed

    private void btnEditarRPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarRPActionPerformed
        HabilitarTextFieldReparaciones();
        txtCantidadPiezasRP.setEnabled(false);
        cbVehiculoRP.setEnabled(false);
        cbPiezaRP.setEnabled(false);
       
        btnNuevoRP.setEnabled(false);
        btnSalvarRP.setEnabled(true);
        btnCancelarRP.setEnabled(true);
        btnEditarRP.setEnabled(false);
        btnRemoverRP.setEnabled(false);
    }//GEN-LAST:event_btnEditarRPActionPerformed

    private void btnRemoverRPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverRPActionPerformed
        Conexion cc=new Conexion();
        Connection cn=cc.conexion();
        String valor = txtDatosRP.getText();
        PreparedStatement pst;
        try {
            pst = cn.prepareStatement("DELETE FROM reparaciones WHERE (REPARACION_ID_RP='"+valor+"')");
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null,"Se eliminó el registro con éxito");
        } catch (SQLException ex) {
             JOptionPane.showMessageDialog(null,"Hubo un error con la eliminación del registro");
        }
        
        try {
            String sql="SELECT * FROM reparaciones";
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                cbIDRPVT.addItem(rs.getString(3));       
            }    
        } catch (SQLException ex) {
        } 
        
        LimpiarReparacion();
        DesHabilitarTextFieldReparaciones();
        NuevoTrueReparacion();
    }//GEN-LAST:event_btnRemoverRPActionPerformed

    private void btnBuscarReparacionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarReparacionesActionPerformed
        cbVehiculoRP.setSelectedIndex(-1);
        cbPiezaRP.setSelectedIndex(-1);
        cbTipoServicioRP.setSelectedIndex(-1);
        try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String valor = txtDatosRP.getText();
            String sql="";
            sql="SELECT * FROM reparaciones WHERE (REPARACION_ID_RP='"+valor+"')";
            
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                cbVehiculoRP.setSelectedItem(rs.getString(1));
                cbPiezaRP.setSelectedItem(rs.getString(2));
                txtIDRP.setText(rs.getString(3));
                txtFallaRP.setText(rs.getString(4));
                txtCantidadPiezasRP.setText(rs.getString(5));
                txtMontoPZ.setText(rs.getString(6));
                txtMontoServicio.setText(rs.getString(7));
                txtFechaENRP.setText(rs.getString(8));           
                txtFechaSARP.setText(rs.getString(9));
                txtEstatusRP.setText(rs.getString(10));      
                cbTipoServicioRP.addItem(rs.getString(11));
                cbTipoServicioRP.setSelectedItem(rs.getString(11));
            }
            if(cbVehiculoRP.getItemAt(cbVehiculoRP.getSelectedIndex()) == null){
                JOptionPane.showMessageDialog(null,"No se encontró el registro a buscar");
                DesHabilitarTextFieldReparaciones();
                LimpiarReparacion();
                NuevoTrueReparacion();
            }else{
                btnNuevoRP.setEnabled(false);
                btnSalvarRP.setEnabled(false);
                btnCancelarRP.setEnabled(false);
                btnEditarRP.setEnabled(true);
                btnRemoverRP.setEnabled(true);
            }  
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,"Hubo un error con la búsqueda");
        }
    }//GEN-LAST:event_btnBuscarReparacionesActionPerformed

    //Pestaña Pieza
    
    private void btnNuevoPiezaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoPiezaActionPerformed
        LimpiarPieza();
        HabilitarTextFieldPiezas();
        btnNuevoPieza.setEnabled(false);
        btnSalvarPieza.setEnabled(true);
        btnCancelarPieza.setEnabled(true);
        btnEditarPieza.setEnabled(false);
        btnRemoverPieza.setEnabled(false);
        
        txtIDPZ.setEnabled(true);
        txtIDPZ.setEditable(true);
    }//GEN-LAST:event_btnNuevoPiezaActionPerformed

    private void btnSalvarPiezaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarPiezaActionPerformed
        boolean valid = false;
        if("".equals(txtDatosPZ.getText())){
            if(ValidarP(txtNombrePZ.getText())){
                valid = true;
                txtNombrePZ.setText("");
                }else{
                    try{
                        int sotck = Integer.parseInt(txtStockPZ.getText());
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(this,"El stock debe ser numerico");
                        valid = true;
                        txtStockPZ.setText("");
                        }
                    }
                    if(valid == false){
                        Conexion cc=new Conexion();
                        Connection cn=cc.conexion();
                        try{
                            PreparedStatement pst=cn.prepareStatement("INSERT INTO piezas(CBARRAS_PZ,NOMBRE_PZ,DESCRIPCION_PZ,STOCK_PZ,PRECIO_PZ) VALUES(?,?,?,?,?)");
                            pst.setLong(1, Long.valueOf(txtIDPZ.getText()));
                            pst.setString(2,txtNombrePZ.getText());
                            pst.setString(3,txtDescripcionPZ.getText());
                            pst.setInt(4,Integer.valueOf(txtStockPZ.getText()));
                            pst.setFloat(5, Float.valueOf(txtPrecioPZ.getText()));

                            int a=pst.executeUpdate();

                            if(a>0){
                                DesHabilitarTextFieldPiezas();
                                LimpiarPieza();
                                NuevoTruePieza();
                                JOptionPane.showMessageDialog(null,"Registro exitoso");
                            }else{
                                DesHabilitarTextFieldPiezas();
                                LimpiarPieza();
                                NuevoTruePieza();
                                JOptionPane.showMessageDialog(null,"Error al agregar");
                            }
                        }catch(Exception e){
                             JOptionPane.showMessageDialog(null,"Error al tratar de agregar");
                        }
                }

        }else if(!"".equals(txtDatosPZ.getText()) && !"".equals(txtNombrePZ.getText())){
                if(ValidarP(txtNombrePZ.getText())){
                    valid = true;
                    txtNombrePZ.setText("");
                }else{
                    try{
                        int sotck = Integer.parseInt(txtStockPZ.getText());
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(this,"El stock debe ser numerico");
                        valid = true;
                        txtStockPZ.setText("");
                    }
                }
                if(valid == false){
                    Conexion cc=new Conexion();
                    Connection cn=cc.conexion();
                    String valor = txtDatosPZ.getText();
                    try{
                        PreparedStatement pst=cn.prepareStatement("UPDATE piezas SET CBARRAS_PZ='"+Long.valueOf(txtIDPZ.getText())+"',NOMBRE_PZ='"+txtNombrePZ.getText()+
                        "',DESCRIPCION_PZ='"+txtDescripcionPZ.getText()+"',STOCK_PZ='"+Integer.valueOf(txtStockPZ.getText())+
                        "',PRECIO_PZ='"+Float.valueOf(txtPrecioPZ.getText())+"' WHERE (CBARRAS_PZ='"+valor+"')");
                        pst.executeUpdate();

                        DesHabilitarTextFieldPiezas();
                        LimpiarPieza();
                        NuevoTruePieza();
                        JOptionPane.showMessageDialog(null,"Actualización exitosa");
                    }catch(Exception e){
                        JOptionPane.showMessageDialog(null,"Hubo un error con la actualización");
                        DesHabilitarTextFieldPiezas();
                        LimpiarPieza();
                        NuevoTruePieza();
                    }
                }
            }
    }//GEN-LAST:event_btnSalvarPiezaActionPerformed

    private void btnCancelarPiezaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarPiezaActionPerformed
        DesHabilitarTextFieldPiezas();
        LimpiarPieza();
        NuevoTruePieza();
    }//GEN-LAST:event_btnCancelarPiezaActionPerformed

    private void btnEditarPiezaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarPiezaActionPerformed
        HabilitarTextFieldPiezas();
        btnNuevoPieza.setEnabled(false);
        btnSalvarPieza.setEnabled(true);
        btnCancelarPieza.setEnabled(true);
        btnEditarPieza.setEnabled(false);
        btnRemoverPieza.setEnabled(false);
    }//GEN-LAST:event_btnEditarPiezaActionPerformed

    private void btnRemoverPiezaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverPiezaActionPerformed
        Conexion cc=new Conexion();
        Connection cn=cc.conexion();
        String valor = txtDatosPZ.getText();
        PreparedStatement pst;
        try {
            pst = cn.prepareStatement("DELETE FROM piezas WHERE (CBARRAS_PZ='"+valor+"')");
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null,"Se eliminó el registro con éxito");
        } catch (SQLException ex) {
             JOptionPane.showMessageDialog(null,"Hubo un error con la eliminación del registro");
        }      
        try {
            String sql="SELECT * FROM piezas";
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                cbPiezaRP.addItem(rs.getString(1));       
            }    
        } catch (SQLException ex) {
        } 
        cbPiezaRP.removeAllItems();
        LimpiarPieza();
        DesHabilitarTextFieldPiezas();
        NuevoTruePieza();
    }//GEN-LAST:event_btnRemoverPiezaActionPerformed

    private void btnBuscarPiezasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarPiezasActionPerformed
        if(!"".equals(txtIDPZ.getText())){
            String datos = txtDatosPZ.getText();
            LimpiarPieza();
            txtDatosPZ.setText(datos);
        }try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String valor = txtDatosPZ.getText();
            String sql="";
            sql="SELECT * FROM piezas WHERE (CBARRAS_PZ='"+valor+"')";
            
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                txtIDPZ.setText(String.valueOf(rs.getLong(1)));
                txtNombrePZ.setText(rs.getString(2));
                txtDescripcionPZ.setText(rs.getString(3));
                txtStockPZ.setText(String.valueOf(rs.getInt(4)));
                txtPrecioPZ.setText(String.valueOf(rs.getString(5)));
            }if("".equals(txtIDPZ.getText())){
                JOptionPane.showMessageDialog(this,"No se encontró el registro a buscar");
                DesHabilitarTextFieldPiezas();
                LimpiarPieza();
                NuevoTruePieza();
            }else{
                btnNuevoPieza.setEnabled(false);
                btnSalvarPieza.setEnabled(false);
                btnCancelarPieza.setEnabled(false);
                btnEditarPieza.setEnabled(true);
                btnRemoverPieza.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,"Hubo un error con la búsqueda");
        }
    }//GEN-LAST:event_btnBuscarPiezasActionPerformed

    //Pestaña Cobro
    
    private void btnNuevoVTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoVTActionPerformed
        LimpiarCobro();
        HabilitarTextFieldCobro();
        btnNuevoVT.setEnabled(false);
        btnSalvarVT.setEnabled(true);
        btnCancelarVT.setEnabled(true);
        btnEditarVT.setEnabled(false);
        btnRemoverVT.setEnabled(false);  
        int ID = 0;
        try {
            Conexion cc=new Conexion();
            Connection cn=cc.conexion();
            String sql="SELECT * FROM cobro";
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                ID = Integer.valueOf(rs.getString(2));
            }  
            ++ID;
            txtIDVT.setText(String.valueOf(ID));   
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,"Hubo un error con los ID auto incrementales");
        } 
        txtFechaCobroVT.setText(GenerarFecha());
        cbTipoPagoVT.addItem("Efectivo");
        cbTipoPagoVT.addItem("Tarjeta");
        txtEstatusCB.setText("PAGADO");    
    }//GEN-LAST:event_btnNuevoVTActionPerformed

    private void btnSalvarVTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarVTActionPerformed
        boolean ban = false;
        if ("".equals(txtDatosVT.getText())) {
            if(cbIDRPVT.getItemAt(cbIDRPVT.getSelectedIndex()) == null){
                JOptionPane.showMessageDialog(null, "Se necesita de una Reparación válida para agregar una reparacion");
                DesHabilitarTextFieldCobro();
                LimpiarCobro();
                NuevoTrueCobro();
            }else{
                if(ban == false){
                    Conexion cc=new Conexion();
                    Connection cn=cc.conexion();
                    try{
                        PreparedStatement pst=cn.prepareStatement("INSERT INTO cobro(ID_RP_CB, ID_CB, CURP_CT_CB, FECHA_COBRO_CB, FECHA_PAGO_CB, MONTO_TOTAL_CB, TIPO_PAGO_CB, ESTATUS_CB) VALUES(?,?,?,?,?,?,?,?)");
                        pst.setInt(1, Integer.valueOf(cbIDRPVT.getSelectedItem().toString()));
                        pst.setInt(2, Integer.valueOf(txtIDVT.getText()));
                        pst.setString(3, txtClienteVT.getText());
                        pst.setString(4, txtFechaCobroVT.getText());
                        pst.setString(5, txtFechaPagoVT.getText());
                        pst.setFloat(6, Float.valueOf(txtTotalVT.getText()));
                        pst.setString(7, cbTipoPagoVT.getSelectedItem().toString());
                        pst.setString(8, txtEstatusCB.getText());
                        int a=pst.executeUpdate();
                        if(a>0){                    
                            String valor = cbIDRPVT.getSelectedItem().toString();
                            String estatus = "PAGADO";
                            try{
                                pst=cn.prepareStatement("UPDATE reparaciones SET ESTATUS_RP='"+estatus+"' WHERE (REPARACION_ID_RP='"+valor+"')");
                                pst.executeUpdate();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(null,"Hubo un error con la actualización");
                            } 
                            DesHabilitarTextFieldCobro();
                            LimpiarCobro();
                            NuevoTrueCobro();
                            JOptionPane.showMessageDialog(null,"Registro exitoso");
                        }else{
                            DesHabilitarTextFieldReparaciones();
                            LimpiarCobro();
                            NuevoTrueCobro();
                            JOptionPane.showMessageDialog(null,"Error al agregar");
                        }
                    }catch(Exception e){
                    }                  
                }
            }
        }else if(!"".equals(txtDatosVT.getText())){
            if(cbIDRPVT.getItemAt(cbIDRPVT.getSelectedIndex()) == null){
                JOptionPane.showMessageDialog(null, "Se necesita de un Reparación válida para agregar una reparacion");
                DesHabilitarTextFieldCobro();
                LimpiarCobro();
                NuevoTrueCobro();
            }else{          
                Conexion cc=new Conexion();
                Connection cn=cc.conexion();
                String valor = txtDatosVT.getText();
                try{
                    String estatus = txtEstatusCB.getText();
                    PreparedStatement pst=cn.prepareStatement("UPDATE cobro SET ID_RP_CB='"+Integer.valueOf(cbIDRPVT.getSelectedItem().toString())+"',ID_CB='"+Integer.valueOf(txtIDVT.getText())+
                    "',CURP_CT_CB='"+txtClienteVT.getText()+"',FECHA_COBRO_CB='"+txtFechaCobroVT.getText()+"',FECHA_PAGO_CB='"+txtFechaPagoVT.getText()+"',MONTO_TOTAL_CB='"+Float.valueOf(txtTotalVT.getText())+
                    "',TIPO_PAGO_CB='"+cbTipoPagoVT.getSelectedItem().toString()+"',ESTATUS_CB='"+estatus+"' WHERE (ID_CB='"+valor+"')");
                    pst.executeUpdate();

                    DesHabilitarTextFieldCobro();
                    LimpiarCobro();
                    NuevoTrueCobro();
                    JOptionPane.showMessageDialog(null,"Actualización exitosa");
                }catch(Exception e){
                    JOptionPane.showMessageDialog(null,"Hubo un error con la actualización");
                    DesHabilitarTextFieldCobro();
                    LimpiarCobro();
                    NuevoTrueCobro();
                }
            }          
        }   
    }//GEN-LAST:event_btnSalvarVTActionPerformed

    private void btnCancelarVTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarVTActionPerformed
        DesHabilitarTextFieldCobro();
        LimpiarCobro();
        NuevoTrueCobro();  
    }//GEN-LAST:event_btnCancelarVTActionPerformed

    private void btnEditarVTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarVTActionPerformed
        HabilitarTextFieldCobro();
        cbIDRPVT.setEnabled(false);
        txtFechaPagoVT.setEnabled(true);
        cbTipoPagoVT.setEnabled(false);
        
        btnNuevoVT.setEnabled(false);
        btnSalvarVT.setEnabled(true);
        btnCancelarVT.setEnabled(true);
        btnEditarVT.setEnabled(false);
        btnRemoverVT.setEnabled(false);
    }//GEN-LAST:event_btnEditarVTActionPerformed

    private void btnRemoverVTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverVTActionPerformed
        Conexion cc=new Conexion();
        Connection cn=cc.conexion();
        String valor = txtDatosVT.getText();
        PreparedStatement pst;
        try {
            pst = cn.prepareStatement("DELETE FROM cobro WHERE (ID_CB='"+valor+"')");
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null,"Se eliminó el registro con éxito");
        } catch (SQLException ex) {
             JOptionPane.showMessageDialog(null,"Hubo un error con la eliminación del registro");
        }
        
        LimpiarCobro();
        DesHabilitarTextFieldCobro();
        NuevoTrueCobro();
    }//GEN-LAST:event_btnRemoverVTActionPerformed

    private void btnBuscarVTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarVTActionPerformed
        cbIDRPVT.setSelectedIndex(-1);
        try {Conexion cc=new Conexion();Connection cn=cc.conexion();
            String valor = txtDatosVT.getText();
            String sql="";
            sql="SELECT * FROM cobro WHERE (ID_CB='"+valor+"')";
            Statement st=cn.createStatement();ResultSet rs=st.executeQuery(sql);
            while(rs.next()){                
                cbIDRPVT.setSelectedItem(String.valueOf(rs.getString(1)));
                if(cbIDRPVT.getItemAt(cbIDRPVT.getSelectedIndex()) == null){
                    cbIDRPVT.addItem(String.valueOf(rs.getString(1)));
                    cbIDRPVT.setSelectedItem(String.valueOf(rs.getString(1)));
                }   
                txtIDVT.setText(String.valueOf(rs.getString(2)));
                txtClienteVT.setText(rs.getString(3));                
                txtFechaCobroVT.setText(rs.getString(4));
                txtFechaPagoVT.setText(rs.getString(5));                
                txtTotalVT.setText(String.valueOf(rs.getString(6)));
                cbTipoPagoVT.addItem(rs.getString(7));               
                txtEstatusCB.setText(rs.getString(8));
            }if("".equals(txtIDVT.getText())){
                JOptionPane.showMessageDialog(null,"No se encontró el registro a buscar");
                DesHabilitarTextFieldCobro();
                LimpiarCobro();
                NuevoTrueCobro();}else{
                DesHabilitarTextFieldCobro();
                btnNuevoVT.setEnabled(false);
                btnSalvarVT.setEnabled(false);
                btnCancelarVT.setEnabled(false);
                btnEditarVT.setEnabled(true);
                btnRemoverVT.setEnabled(true);}          
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,"Hubo un error con la búsqueda");
        }
    }//GEN-LAST:event_btnBuscarVTActionPerformed

    //Reparaciones
    
    private void txtCantidadPiezasRPKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantidadPiezasRPKeyReleased
        if(cbPiezaRP.getItemAt(cbPiezaRP.getSelectedIndex()) != null){
            float precio = 0;
            try {
                Conexion cc=new Conexion();
                Connection cn=cc.conexion();
                String valor = cbPiezaRP.getSelectedItem().toString();
                String sql="SELECT * FROM piezas WHERE (CBARRAS_PZ='"+valor+"')";
                Statement st=cn.createStatement();
                ResultSet rs=st.executeQuery(sql);
                while(rs.next()){
                    precio = Float.valueOf(rs.getString(5));
                }  
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this,"Hubo un error con la búsqueda");
            }
            int cantidad = 0; 
            if("".equals(txtCantidadPiezasRP.getText())){
                cantidad = 0;
            }else{
                cantidad = Integer.valueOf(txtCantidadPiezasRP.getText());
            }
            float monto = 0;
            monto = cantidad * precio;
            DecimalFormat df = new DecimalFormat(".00");
            txtMontoPZ.setText(String.valueOf(df.format(monto)));
        }
    }//GEN-LAST:event_txtCantidadPiezasRPKeyReleased

    //Cobro
    
    private void cbIDRPVTItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbIDRPVTItemStateChanged
        if(cbIDRPVT.isEnabled()){
            String id_vh = "",curp = "";float montoPZ = 0, montoServicio = 0, total = 0;
            try{
                Conexion cc=new Conexion();
                Connection cn=cc.conexion();
                String valor = cbIDRPVT.getSelectedItem().toString();
                String sql="SELECT * FROM reparaciones WHERE (REPARACION_ID_RP='"+valor+"')";

                Statement st=cn.createStatement();
                ResultSet rs=st.executeQuery(sql);
                while(rs.next()){
                    id_vh = rs.getString(1);
                    montoPZ = Float.valueOf(rs.getString(6));
                    montoServicio = Float.valueOf(rs.getString(7));
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this,"Hubo un error con la búsqueda");
            }
            try{
                Conexion cc=new Conexion();
                Connection cn=cc.conexion();
                String sql="SELECT * FROM vehiculos WHERE (MATRICULA_VH='"+id_vh+"')";
                Statement st=cn.createStatement();
                ResultSet rs=st.executeQuery(sql);
                while(rs.next()){
                    curp = rs.getString(1);
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this,"Hubo un error con la búsqueda");
            }
            txtClienteVT.setText(curp);
            total = montoServicio + montoPZ;
            DecimalFormat df = new DecimalFormat(".00");
            txtTotalVT.setText(String.valueOf(df.format(total)));
        }
    }//GEN-LAST:event_cbIDRPVTItemStateChanged

    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarCliente;
    private javax.swing.JButton btnBuscarPiezas;
    private javax.swing.JButton btnBuscarReparaciones;
    private javax.swing.JButton btnBuscarUsuario;
    private javax.swing.JButton btnBuscarVT;
    private javax.swing.JButton btnBuscarVehiculo;
    private javax.swing.JButton btnCancelarCliente;
    private javax.swing.JButton btnCancelarPieza;
    private javax.swing.JButton btnCancelarRP;
    private javax.swing.JButton btnCancelarUsuario;
    private javax.swing.JButton btnCancelarVT;
    private javax.swing.JButton btnCancelarVehiculo;
    private javax.swing.JButton btnEditarCliente;
    private javax.swing.JButton btnEditarPieza;
    private javax.swing.JButton btnEditarRP;
    private javax.swing.JButton btnEditarUsuario;
    private javax.swing.JButton btnEditarVT;
    private javax.swing.JButton btnEditarVehiculo;
    private javax.swing.JButton btnNuevoCliente;
    private javax.swing.JButton btnNuevoPieza;
    private javax.swing.JButton btnNuevoRP;
    private javax.swing.JButton btnNuevoUsuario;
    private javax.swing.JButton btnNuevoVT;
    private javax.swing.JButton btnNuevoVehiculo;
    private javax.swing.JButton btnRemoverCliente;
    private javax.swing.JButton btnRemoverPieza;
    private javax.swing.JButton btnRemoverRP;
    private javax.swing.JButton btnRemoverUsuario;
    private javax.swing.JButton btnRemoverVT;
    private javax.swing.JButton btnRemoverVehiculo;
    private javax.swing.JButton btnSalvarCliente;
    private javax.swing.JButton btnSalvarPieza;
    private javax.swing.JButton btnSalvarRP;
    private javax.swing.JButton btnSalvarUsuario;
    private javax.swing.JButton btnSalvarVT;
    private javax.swing.JButton btnSalvarVehiculo;
    private javax.swing.JComboBox<String> cbIDClienteVH;
    private javax.swing.JComboBox<String> cbIDRPVT;
    private javax.swing.JComboBox<String> cbIDUsuarioCT;
    private javax.swing.JComboBox<String> cbPiezaRP;
    private javax.swing.JComboBox<String> cbTipoPagoVT;
    private javax.swing.JComboBox<String> cbTipoServicioRP;
    private javax.swing.JComboBox cbVehiculoRP;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lbl;
    private javax.swing.JLabel lblAMClientes;
    private javax.swing.JLabel lblAMUsuarios;
    private javax.swing.JLabel lblAPClientes;
    private javax.swing.JLabel lblAPUsuarios;
    private javax.swing.JLabel lblBuscarUsuarios;
    private javax.swing.JLabel lblCantidadPiezasReparaciones;
    private javax.swing.JLabel lblClienteVT;
    private javax.swing.JLabel lblDatosVT;
    private javax.swing.JLabel lblDescripcionPieza;
    private javax.swing.JLabel lblDireccionCT;
    private javax.swing.JLabel lblDireccionUsuarios;
    private javax.swing.JLabel lblEstatusCB;
    private javax.swing.JLabel lblEstatusRP;
    private javax.swing.JLabel lblFallaReparaciones;
    private javax.swing.JLabel lblFechaAtencionCT;
    private javax.swing.JLabel lblFechaEntradaReparaciones;
    private javax.swing.JLabel lblFechaPago;
    private javax.swing.JLabel lblFechaSalidaReparaciones;
    private javax.swing.JLabel lblFechaVT;
    private javax.swing.JLabel lblFechaVehiculo;
    private javax.swing.JLabel lblFondoUS;
    private javax.swing.JLabel lblIDBuscarClientes;
    private javax.swing.JLabel lblIDBuscarPieza;
    private javax.swing.JLabel lblIDCliente;
    private javax.swing.JLabel lblIDClientesUsuario;
    private javax.swing.JLabel lblIDPieza;
    private javax.swing.JLabel lblIDUsuarios;
    private javax.swing.JLabel lblIDVT;
    private javax.swing.JLabel lblIDVehiculoReparaciones;
    private javax.swing.JLabel lblIcon;
    private javax.swing.JLabel lblMarcaVehiculo;
    private javax.swing.JLabel lblMatriculaVehiculo;
    private javax.swing.JLabel lblModeloVehiculo;
    private javax.swing.JLabel lblMonto1;
    private javax.swing.JLabel lblMonto2;
    private javax.swing.JLabel lblNOMBREODICT;
    private javax.swing.JLabel lblNOMBREOIDRP;
    private javax.swing.JLabel lblNOMBREOIDVH;
    private javax.swing.JLabel lblNombreCliente;
    private javax.swing.JLabel lblNombrePZ;
    private javax.swing.JLabel lblNombreUS;
    private javax.swing.JLabel lblPiezaIDreaparaciones;
    private javax.swing.JLabel lblPrecioPZ;
    private javax.swing.JLabel lblReparacionID;
    private javax.swing.JLabel lblReparacionIDVT;
    private javax.swing.JLabel lblSeleccionarVehiculo;
    private javax.swing.JLabel lblStockPieza;
    private javax.swing.JLabel lblTelefonoUsuarios;
    private javax.swing.JLabel lblTipoPago;
    private javax.swing.JLabel lblTipoServicio;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JTextField txtAMCT;
    private javax.swing.JTextField txtAMUS;
    private javax.swing.JTextField txtAPCT;
    private javax.swing.JTextField txtAPUS;
    private javax.swing.JTextField txtCURPCT;
    private javax.swing.JTextField txtCantidadPiezasRP;
    private javax.swing.JTextField txtClienteVT;
    private javax.swing.JTextField txtDatosCT;
    private javax.swing.JTextField txtDatosPZ;
    private javax.swing.JTextField txtDatosRP;
    private javax.swing.JTextField txtDatosUS;
    private javax.swing.JTextField txtDatosVH;
    private javax.swing.JTextField txtDatosVT;
    private javax.swing.JTextField txtDescripcionPZ;
    private javax.swing.JTextField txtDireccionCT;
    private javax.swing.JTextField txtDireccionUS;
    private javax.swing.JTextField txtEstatusCB;
    private javax.swing.JTextField txtEstatusRP;
    private javax.swing.JTextField txtFallaRP;
    private javax.swing.JTextField txtFechaCT;
    private javax.swing.JTextField txtFechaCobroVT;
    private javax.swing.JTextField txtFechaENRP;
    private javax.swing.JTextField txtFechaPagoVT;
    private javax.swing.JTextField txtFechaSARP;
    private javax.swing.JTextField txtFechaVH;
    private javax.swing.JTextField txtIDPZ;
    private javax.swing.JTextField txtIDRP;
    private javax.swing.JTextField txtIDUS;
    private javax.swing.JTextField txtIDVT;
    private javax.swing.JTextField txtMarcaVH;
    private javax.swing.JTextField txtMatriculaVH;
    private javax.swing.JTextField txtModeloVH;
    private javax.swing.JTextField txtMontoPZ;
    private javax.swing.JTextField txtMontoServicio;
    private javax.swing.JTextField txtNombreCT;
    private javax.swing.JTextField txtNombrePZ;
    private javax.swing.JTextField txtNombreUS;
    private javax.swing.JTextField txtPrecioPZ;
    private javax.swing.JTextField txtStockPZ;
    private javax.swing.JTextField txtTelefonoUS;
    private javax.swing.JTextField txtTotalVT;
    // End of variables declaration//GEN-END:variables



}
